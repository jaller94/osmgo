// Roof.js

PlaceRoof = function(way,height,points,materialX,points, min_height) { // Render function /// bbb bbb bbb bbb bbb bbb bbb bbb bbb bbb bbb bbb bbb 

    var shape    = new THREE.Shape( points )
	var roofLevels = false


    if(  !(roofShape=way.GetTag("roof:shape"         ))
      && !(roofShape=way.GetTag("building:roof:shape"))
       )
    {
        //var small = Math.sqrt( shape.getLength() )  < 10
        //if(!small)
        return 0
        //roofShape = "gabled"
    }	

	var maxx = maxz = -1e9
	var minx = minz	= +1e9
	for (var i in points) {
		maxx = Math.max(maxx,points[i].x)
		maxz = Math.max(maxz,points[i].y)
		minx = Math.min(minx,points[i].x)
		minz = Math.min(minz,points[i].y)	}
	var x0 = (minx+maxx)/2   // Mittelpunkt Meter-absolut
	var z0 = (minz+maxz)/2

	var	roofHeight = way.GetTag("roof:height",0)*1
  //var heightPar = height

	if( roofHeight== 0) {
		roofHeight = way.GetTag("roof:levels",0)*levelHeight
		if( roofHeight!=0) {
			height    += roofHeight
			roofLevels = true
		}
	}

	var across = way.GetTag("roof:orientation")=="across"

	if(roofHeight>height) roofHeight = height
	
	switch(roofShape)
	{
	case "flat": /////////////////////////////////////////// fff
		return undefined  // default
		
	case "pyramidal": ////////////////////////////////////// ppp
		// log("roof:shape/height",roofShape,roofHeight)
		if(roofHeight==0) {
	   		roofHeight = (maxx-minx)/2
	   		if(roofHeight>height/2) roofHeight = height/2
		}

		var p=0
	    var geometry = new THREE.Geometry()
		    geometry.vertices.push(      new THREE.Vector3(0,0,0) ) // zerro.point at the upper dip     // Meter-relativ
		for (p in points) {
		    geometry.vertices.push(      new THREE.Vector3( points[p].x-x0, -roofHeight,
															points[p].y-z0  ) )    // Meter-relaitv
			if(p*1>0) geometry.faces.push( new THREE.Face3( 0, p*1, p*1+1 ) )
		}   //      geometry.faces.push( new THREE.Face3( 0, p*1, p*1+1 ) )
	    geometry.computeFaceNormals();   // Flächenfarbe einrichten
	  //geometry.computeVertexNormals(); // Farbübergänge weich
	    geometry.mergeVertices()         // Nach V.Normals ist das ok :-)
	    var material = materialX[0]
	    if(roofLevels) roofHeight = 0 // Wenn die Dachhöhe in Level ist, kommt das Dach Über, nicht ins Haus

		assignUVs(geometry)
	  	break



  case "dome": ///////////////////////////////////////////// ddd
		// log("roof:shape/height",roofShape,roofHeight)
  		if(roofHeight==0) {
    	 	roofHeight = (maxx-minx)/2
    	 	if(roofHeight>height/2) roofHeight = height/2
  		}

		var geometry = new THREE.Geometry()
		var v = 0 // verticle counter
		var c = 6
		var p = 0
		for (p in points) { // all points of a ring
			// calculate angel center to side-point
    		var x  = (points[p].x-x0)
    		var z  = (points[p].y-z0)
			for(var a=0 ; a<=90 ; a+=(90/6) ) {// all angles
				var sc = SinCos(g(a))
				geometry.vertices.push( new THREE.Vector3( x*sc[1] , -roofHeight*(1-sc[0]) , z*sc[1] )) ;v++
				if(p>0 && a>0) {
					geometry.faces.push( new THREE.Face3( v-1, v-2, v-c-2        ) )
					geometry.faces.push( new THREE.Face3(      v-2, v-c-2, v-c-3 ) )
				}
			}  // a
		} // p
	    var material = materialX[1]
	    if(roofLevels) roofHeight = 0 // Wenn die Dachhöhe in Level ist, kommt das Dach Über, nicht ins Haus

		assignUVs(geometry)
		break


	case "zwiebelXXX":
		// log("roof:shape/height",roofShape,roofHeight)

		if(roofHeight==0) {
	   		roofHeight = (maxx-minx)/2
	   		if(roofHeight>height/2) roofHeight = height/2
		}

	    var Segmente = 24
	    var Kurve = /*  // |y|  -x-  0,0 ist impliziert im code */
	        [ [  7, 58],[ 12,115],[ 65,215],[ 125,296],[ 200,364],[ 281,416],[ 381,455],[ 478,473],[ 567,478],[ 666,472],
	          [748,456],[873,414],[968,365],[1048,312],[1111,259],[1170,200],[1215,146],[1256, 81],[1270, 41],[1273,  0] ];
		var v = -2;
		var geometry = new THREE.Geometry()
		for(var s=0; s<(Segmente); s++) {
			geometry.vertices.push( new THREE.Vector3(0,0,0), new THREE.Vector3(0,0,0)); v+=2; // vertices 0 und 1: Die unteren Punkte des Kugel-Segments sind beide 0,0
			var a1 = g(360)/Segmente*(s+0.5);   // Winkel der Rechten/Linken Kante
			var a2 = g(360)/Segmente*(s-0.5);
			//r(var i=0; i<Kurve.length; i++) {
			for(var i in Kurve) {                                                                
				var y  = Kurve[i][0] / 1273;  // durch Maximalwert der Kurve gibt es eine Normierung auf 1
				var x  = Kurve[i][1] / 478/2;
				var sc1= SinCos(a1,x);
				var sc2= SinCos(a2,x);
			geometry.vertices.push( new THREE.Vector3(sc1[0],y,sc1[1]));
			geometry.vertices.push( new THREE.Vector3(sc2[0],y,sc2[1]));
			
			geometry.faces.push( new THREE.Face3( v+0, v+2, v+1 ) );
			geometry.faces.push( new THREE.Face3( v+1, v+2, v+3 ) );
			v+=2; // log("face:",i,Kurve.length)
			}  // i
		} // s
	    var material = materialX[1]
	    if(roofLevels) roofHeight = 0 // Wenn die Dachhöhe in Level ist, kommt das Dach Über, nicht ins Haus

		assignUVs(geometry)
		break




	case "skillion": //// Roof will replace the whole building!
		//log("roof skillion id: ",way.id)
		// Hier wird das genze Gebäude samt Dach erzeugt! Erst als Flachdach, dann ...

		if(roofHeight==0) {
	   		roofHeight = (maxx-minx)/10
	   		if(roofHeight>height/2) roofHeight = height/2
		}

		var m = new THREE.Matrix4()
		var geometry = new THREE.ExtrudeGeometry( shape, {amount: height/*-min_height*/, bevelEnabled: false } )
		    geometry.applyMatrix( m.makeRotationX(  g(90)) )
			x0 = z0 = 0 // ist schon Meter-Absolut

		// ... dann werden die Wände gesenkt - sehr getrixt!
		// log("id points 0 1 x y:",way.id,points[0].x,points[0].y,points[1].x,points[1].y)

		var maxs = 0
		var b  // Scan vor the longest wall
		for(var i=0; i<points.length-1; i++) {
			var x = (points[i].x-points[i+1].x)
    		var z = (points[i].y-points[i+1].y)
			var s = Phytagoras(x,z)
			if( maxs < s) {
				maxs = s
    			b = Math.atan2(x,z) - g(180)   //;log("id x,z,r b",way.id, x,z,r(b)) // Angle of building
			}
		}

		if(across)
			b += g(90)

		var d = way.GetTag("roof:slope:direction")
			if(d) b = g(90-d)
		var d = way.GetTag("roof:direction")
			if(d) b = g(90-d)
				
			if(b>g(360)) b-= g(360)
			if(b<g(  0)) b+= g(360)

		    geometry.applyMatrix( m.makeRotationY(-b) )

			var maxx = -1e9  // Höchste und niedrigste Stelle ermitteln
			var minx = +1e9
			for(var i=0; i<geometry.vertices.length/2; i++) {
				maxx = Math.max(maxx,geometry.vertices[i].x)
				minx = Math.min(minx,geometry.vertices[i].x)
			}
			var dm = roofHeight/(maxx-minx) // Höhen/Tiefe der Nodes/Ecken berechenen
			var v2 = geometry.vertices.length/2
			for(var i=0; i<v2; i++) {
				var h3 = (geometry.vertices[i].x - minx) * dm
				geometry.vertices[i].y = -h3
			//	geometry.vertices[i+v2].y = -(height-min_height)
			}

		    geometry.applyMatrix( m.makeRotationY(+b) )


		var material   = materialX	;if(THREE.REVISION<=83)
			material   = new THREE.MultiMaterial(materialX)
			roofHeight = height // Damit nur die roof-geometry erscheint
		break
		






		
		
		
		
		
	// file:///Users/Karl/Dropbox/OSMgo/act/go.html?lat=49.71373&lon=11.08986&ele=32.75&dir=312&view=-35&user=karlos&tiles=1
	
	case "gabledXXX": 
		
		// if(way.id!=442474125) return
		if(roofHeight==0) {
	   		roofHeight = (maxx-minx)/3
	   		if(roofHeight>height/2) roofHeight = height/2
		}

		var m = new THREE.Matrix4()
		var geometry = new THREE.ExtrudeGeometry( shape, {amount: height/*-min_height*/, bevelEnabled: false } )
		    geometry.applyMatrix( m.makeRotationX(  g(90)) )

		var revert 	=  points[1].x != geometry.vertices[1].x 
					|| points[1].y != geometry.vertices[1].z

		// ... dann werden die Wände gesenkt - sehr getrixt!
		// log("id points 0 1 x y:",way.id,points[0].x,points[0].y,points[1].x,points[1].y)

		var maxs = 0
		var b  // Scan vor the longest wall

		// Richtung des Dachfirsts erkennen
		for(var i=0; i<points.length-1; i++) {
			var x = (points[i].x-points[i+1].x)
    		var z = (points[i].y-points[i+1].y)
			var s = Phytagoras(x,z)
			if( maxs < s) {
				maxs = s
    			b = Math.atan2(x,z) - g(180)   //;log("id x,z,r b",way.id, x,z,r(b)) // Angle of building
				j = i
			}
		}

		if(across)
			b += g(90)

		var d = way.GetTag("roof:slope:direction")
			if(d) b = g(90-d)

			//log("roof gabled id: ", way.id, r(b) )

			while(b<0) b+= g(180)

			// Drehen um die Firstpunkte zu finden
		    geometry.applyMatrix( m.makeRotationY(-b) )

			var maxx = maxz = -1e9  // Höchste und niedrigste Stelle ermitteln
			var minx = minz = +1e9
			for(var i=0; i<geometry.vertices.length/2; i++) {
				maxx = Math.max(maxx,geometry.vertices[i].x)
				minx = Math.min(minx,geometry.vertices[i].x)
				maxz = Math.max(maxz,geometry.vertices[i].z)
				minz = Math.min(minz,geometry.vertices[i].z)
			}
			var midx = (maxx-minx)/2+minx  // Giebelposition!
			var midz = (maxz-minz)/2+minz

			// Welche Wand ist unterm Gibel und wo?
			var l2 = geometry.vertices.length/2
			for(var i=0; i<l2; i++) {
				var x0 = geometry.vertices[i+0].x
				var x1 = geometry.vertices[i+1].x 
				var z0 = geometry.vertices[i+0].z
				var z1 = geometry.vertices[i+1].z

				if(	 Math.sign(x0-midx)
				  != Math.sign(x1-midx)
				   ) { // Drunter!
					var fakt = (x1-x0)  /  ((x1-x0)/2-x0) // Faktor für Anteil zur ersten Node (vorzeichenrelevant!)
					var difz = Math.abs(z0-z1)	 // Wandecken-Abstand Z
					var zFir = difz*fakt + z0 	 // Erste-First-Abstand z + Erste = First z am Kreuzungspunkt
	//					if(zFir>midz) zFir*=1.01
	//					else          zFir*=0.99
					// points.splice(i+1+inserts, 0, new THREE.Vector2(midx,zFir)) // First-Point in die mitte einfügen 
					var v = new THREE.Vector3(midx,0,zFir)  ;v.index = i+1 // Index in Punktearray merken
					geometry.vertices.push(v)  // Punkte hinten dazu damit sie auch zurück gedreht werden
				}//drunter!
			}//for
		    geometry.applyMatrix( m.makeRotationY(+b) )
			// Neue Punkte einbauen (von hinten)
			if(revert) points.reverse()
			do {
				var v = geometry.vertices.pop()
				if(v.index) {
					var p = new THREE.Vector2(v.x,v.z)
					points.splice(v.index, 0, p) // First-Point in die mitte einfügen 
				}
			} while (v.index)
			if(revert) points.reverse()

			// Die Hausecken sind jetzt auch beim First. Extrude neu anlegen und drehen
			shape    = new THREE.Shape( points )
		    geometry = new THREE.ExtrudeGeometry( shape, {amount: height/*-min_height*/, bevelEnabled: false } )
		    geometry.applyMatrix( m.makeRotationX(  g(90)) )
		    geometry.applyMatrix( m.makeRotationY(-b) )

			var dm = roofHeight/(midx-minx) // Höhen/Tiefe der Nodes/Ecken berechenen
			for(var i=0; i<geometry.vertices.length/2; i++) {
				var h3 = Math.abs(geometry.vertices[i].x - midx) * dm
				geometry.vertices[i].y = -h3
			}

		    geometry.applyMatrix( m.makeRotationY(+b) )
			
		    geometry.verticesNeedUpdate
		  //geometry.elementsNeedUpdate 



		var material    = materialX;	if(THREE.REVISION<=83)
			material    = new THREE.MultiMaterial(materialX)
			roofHeight = height // Damit nur die roof-geometry erscheint
			x0 = z0 = 0 // ist schon Meter-Absolut
		//  if(roofLevels) roofHeight = heightPar // Originalwert, damit unten 0 extruded wird 

		break // gabled






		

	default:
		// log("Rooftype not supported! shape/height: ",roofShape,roofHeight)
	  	return 0
	}

		if(geometry.faces.length!=geometry.faceVertexUvs[0].length)
			alert("mist vertex roof")


	var mesh     = new THREE.Mesh( geometry, material )
    	mesh.position.x = x0 // Meter-absolut
    	mesh.position.z = z0
    	mesh.position.y = height // Über Building-Haupttteil
		mesh.castShadow = mesh.receiveShadow = shadow
		mesh.osm = way
		mesh.roofHeight = roofHeight
	
	return mesh
}




/////////////////////////////////



function assignUVs(geometry) {

    geometry.faceVertexUvs[0] = [];

    geometry.faces.forEach(function(face) {

        var components = ['x', 'y', 'z'].sort(function(a, b) {
            return Math.abs(face.normal[a]) > Math.abs(face.normal[b]);
        });

        var v1 = geometry.vertices[face.a];
        var v2 = geometry.vertices[face.b];
        var v3 = geometry.vertices[face.c];

        geometry.faceVertexUvs[0].push([
            new THREE.Vector2(v1[components[0]], v1[components[1]]),
            new THREE.Vector2(v2[components[0]], v2[components[1]]),
            new THREE.Vector2(v3[components[0]], v3[components[1]])
        ]);

    });

    geometry.uvsNeedUpdate = true;
}