//-----------------------------------------------------------------------------
// Karlos Orion-Baustelle


var vscDemoObj = undefined;
var Boxes;
var BoxFrames;
var Name = "Karls";


var tile = 0; // Kachel // VxObj

var params="d";
	
//######### + Orion.js ###############################################################################################################


	var aa  = 25;
	var bb  =  8;
	var cc  =  1;




function contains(a, obj) {
  for (var i = 0; i < a.length; i++)  {
    if (a[i] === obj)  return true; }
  return false;
}


function g(grad) {   //   Vollkreis 360 in Rad wandeln
  return grad / 360 * 2*Math.PI;
}


function SinCos(a,r){
  return [Math.sin(a)*r,Math.cos(a)*r];
}


function go(check) {  // returns true (=Darstellen) wenn URL-Parameterstring Zeichen "check" enth?lt
  if(params=="") return true; // Kein String? Alles darstellen
  if(params.indexOf(check)>=0) return true; // gefunden
  return false;
}


//// I N I T ////////////////////////////////////////////////////////////////////////////////////////////////////////


var adjust = 0;

function Orion_Init(){// Initziierungen

  // White directional light at half intensity shining.
/*
  var lightA = new THREE.AmbientLight(     0x111111     );                                    scene.add( lightA );
      lightO = new THREE.SpotLight(        0xccCCcc,1.5 ); lightO.position.set(  8,2.3,-33 ); scene.add( lightO );  lightO.castShadow = true;
  var lightQ = new THREE.DirectionalLight( 0xccCCcc,0.9 ); lightQ.position.set( -2, -3, -2 ); scene.add( lightQ );  lightQ.castShadow = true;
  var lightR = new THREE.DirectionalLight( 0xccCCcc,0.9 ); lightR.position.set(  2,  3,  2 ); scene.add( lightR );  lightR.castShadow = true;
  var lightP = new THREE.DirectionalLight( 0xccCCcc,0.9 ); lightP.position.set( -22,  3, 22); scene.add( lightP );  lightP.castShadow = true;
*/
   // lightO.shadowCameraVisible = true;   /// http://learningthreejs.com/blog/2012/01/20/casting-shadows/
   // !! Wenn ich THREE.SpotLight benutze, komm  "THREE.WebGLProgram: gl.getProgramInfoLog()" "(69,6): warning X3557: loop only executes for 1 iteration(s), forcing loop to unroll" -- Bekannt. Behoben? Bei mir nicht?  https://github.com/mrdoob/three.js/issues/4834
  

  //// Dimensions (Sizes, Distances, Positions) /////////////////////////////////////////////////////////////////////////
  // Alle (?bergreifend?) verwendeten Ma?e sind hier versammelt (Ohne "var" werden sie "global")
  // Das ist kein guter Stiel; mal sehen ...

  AchtA  = 1.0823922002925388 //    / 2.414213562373 * 2.613125929753;  Achteckumrechinung innen=>aussen

  TopY   = 2.3;
  TopYD  = 0.02*2; // Zwei mal 5cm Holz???  Oder 2*2.5?
  TopR   = 8.8/2;  // Radius der Decke.
  TopK   = 2.10;   // Abstand Decke V-W?nde (graue "Momo"-K?sten) ! K?sten und W?nde sind Achteckig, aber die Position bezieht sich auf die Kante des Achtecks,also dem Deckenradius

  HowlR  = 1.8/2;
  HowlS  = 1.6;     // Spin-Radius
  LiftR  = 1.0/2;
  LifCR  = 1.40/2;   // Radius des Conus-Ring am Lift oben  1.4?
  LifCY  = 0.18;     // 0.3?     
  LiftS  = 3.25;
  ConeRo = 0.20/2;

  FloorYD = TopYD;
  FloorY  =  -0.6;
  FloorR  =  14.5 * AchtA /2;
  HowlFR  =  1.8/2;
  HowlFS  =  3.3; // Loch 136p1=1,8m  +  Abstand 112p=1,48

  // Globale Variablen sind noch menr Pfui als Konstatnen //////////////////////////
  conen = 0;
  DateLast=Date.now(), vLift = +0.01;
  turn = 0;
  mStachelei = null;
  mComputer  = null;

  //// Material (Color,Transparency) ///////////////////////////////////////////////////////////////////////////////////

  // Hilfsfuntion zur Auswahl von Farbpaletten und SW-Darstellung
  function Mat(Josef, Karl, options )  // 3 Parameter: Josef: Farbe nach Hilger, Karl: Farbe nach Adler, ggf=Josef, options: z.B Durchsichtig
  {
    var c = 0x808080;
    if(Karl=-1) Karl=Josef;  // Wenn man options an gibt, muss man auch Karl angeben. -1 bedeutet, das der Wert von Josef ?bernommen wird.
    switch(Math.abs(MatMode)) // Farbauswahl ?ber Variable (Konstante oder HTML-Parameter
    { case 1:  c = Josef;   break;
      case 2:  c = Karl;    break; }
    if(MatMode<0) // Bei negativer Wahl:
    { // Bunt in SW umrechnen
      var r = Math.floor( c /   0x10000);
          c =             c - r*0x10000;
      var g = Math.floor( c /     0x100);
      var b =             c - g*  0x100;
          c = Math.floor( (r+g+b)/3 ); // SW-Mittelwert
          c = c*0x10000 + c*0x100 + c;
    }
    if(options==null) // Ohne Optionen?
         return new THREE.MeshLambertMaterial( {color: c} );    
    else // Mit Optioen: Farbe und Sontiges zusammenfassen zu einem Objekt
    {    var cc = {color: c};
         for (var attrname in options) { cc[attrname] = options[attrname]; }
         return new THREE.MeshLambertMaterial( cc );
    }
  }

  // Alle Farben sind hier zusammengefa?t. Mal sehen ob das so bleibt.
  // ToDo: Eine Funktion, die die Farben variiert: Leuchten, SchwarzWeis ein/aus
  // 00 .. FF   = dark .. light

  MatMode = 2;  // 1/2/3:Josef/Karl/Reserve/...  >0:Farbe <0:SchwarzWeis  0:???

  // Aufz?ge, Schalttische, Komputer, Decke und Boden:  
  SilberGrauBlau = 0x7091A5;//="rgb(112,145,165)" oder 95,162,194 = 128-33,+34,+74 /2= -16,17,37 +128= 112,145,165

  cTop    = Mat( SilberGrauBlau, 0xbac4c5, {reflectivity: 0.75} ); //=193/0xc1
  cFloor  = Mat( SilberGrauBlau, 0x6e8181, {reflectivity: 0.25} );
  cLift   = Mat( SilberGrauBlau, 0x847e88, {reflectivity: 0.25} );
  cRinge  = Mat( SilberGrauBlau, 0xbbc4c4); //  Deckenlochringe
  cLifC   = Mat( SilberGrauBlau, 0x808080);
  cLifB   = Mat( SilberGrauBlau, 0x666666);
  // Spezielles
  cRed    = Mat( 0xff0000);
  cYellow = Mat( 0xe0e000,0xe0e000,{transparent: true, opacity: 0.90, emissive: 0xe0e080} );
  cLight  = Mat( 0xFFFFFF); // Konnenloch, Hallenboden 
  cGrey   = Mat( 0x888888); 
  cLamellen=Mat( 0x333333); 
  cDark   = Mat( 0x181818); // LiftGriffe
  cBlack  = Mat( 0x000000,-1,{side: THREE.DoubleSide}); // Blende hinter Lamellen   // BackSide/DoubleSide/FrontSide
  
  //glass = new THREE.MeshBasicMaterial(   {color: 0x223344, transparent: true, opacity: 0.75, combine: THREE.MixOperation, reflectivity: 0.25, envMap: textureCube } ),
  cCone   = Mat( 0xf0f0f0,-1,{transparent: true, opacity: 0.5} );  
  cGlas   = Mat( 0xf0f0f0,-1,{transparent: true, opacity: 0.6} );  
  cMatt   = Mat( 0xf0f0f0,-1,{transparent: true, opacity: 0.9} );  
  cWeis   = Mat( 0xFFFFFF);  // Wandecken
  cWand   = Mat( 0xAE5700);  //  0xa89b7a b4a584   unbeleuchtet:RGB 174,87,0  beleuchtet:232,192,89
  cEi     = Mat( 0xf0f0f0,-1,{transparent: true, opacity: 0.8, side: THREE.DoubleSide} ); // 0.97  
  cEiStr  = Mat( SilberGrauBlau,-1,                           {side: THREE.DoubleSide} );  
  cEiAn   = Mat( 0x666666); 
  cMetal  = new THREE.MeshPhongMaterial(   {color: SilberGrauBlau, reflectivity: .8 , side: THREE.DoubleSide});
    
}// Initziierungen


////////////////////////////////////////////////////////////////////////////                        
{ ///  "Bauen-Lib" Anfang  /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////                        


function Spin(div,min,max,phase,func) {  // div=Kreisteil
// Spin: Drehung eines Vektors  auf dem Objekte platziert werden (um eine beliebige Achse)
// Todo:Beschreiben
  var rad   = g(360) / div;  // Schrittweite in rad. Der Parameter "div" ist die Anzahl der Schritte f?r 360?
                             // min: Anfangs der Schritte (als Nummer, darf auch negativ sein)
  if(max==0) max = div-1;    // Ende der Schritte. Ggf. gleich "Alle"
  for( var step = min; step<=max ; step++ ) {
    var angle = step*rad + phase;  // Winkel errechen
    func(angle,step);
  }

} // Ende Spin ///


function new_mesh_snap(geometry, material, mode, to, angle,x,z,rx) { /*****  x>r Radius(nichtRotation!)  z>s Shift  rx>ax  */
/*****
Da das hier mal Teil einer Bauen-Lib werden soll, mal beschreiben:
Erzeugen eines NEUEN Objekts relativ zu einem Anderen (Parameter: "to")
* Relativ kann verschiedenes sein. Mit dem "mode" Zeichen kann man angeben: v^<>o*.
  - "unterhalb" oder "oberhalb" wobei die H?he vom "to" und NEUEN ber?cksichtigt werden.
  - Todo: links/rechts, vorne/hinten, mitte, was gebraucht wird
  "to" kann auch ein imagin?res Object3D oder Punkt(ToDo) im Raum sein, dann ist die L?nge/Breite/H?he 0
* F?r das NEUE werden Parameter "geometry" und ein "material" ben?tigt. Wenn da nichts ?bergeben wird, 
  wird nur ein immagin?res Object3D erzeugt. Das neue kann auch schon ein Mesh sein.
* Das NEUE wird child des "to". Daher sind auch die Positionsangaben immer Relativ zum "to"!
  Das NEUE muss nicht mittig zum "to" entstehen, es gibt die Parameter "angle", "x" "z"
  Quasi erst eine Verdrehung der Richtung, und/oder eine Verschiebung. (ToDo: Vector 3 verwenden)
  Damit gehen absolute und kreisf?rmige Positionen.
* Es gibt 2 Gr?nde, ein Mash zu drehen:
  Manchmal ist Oben und Vorne eben nicht in der Ausrichtung, in der eine neue Geometry entsteht
  Meist rotiert um die X-Achse. Der Parameter "rx" ist optional.
  ?fter will man das Objekt im Kreis platzieren, zum Mittelpunkt im er gleich ausgerichtet "angle".
**** ssss */

  // v^<>o*.
  var dx = 0;  var dy = 0;  var dz = 0;

  if(to instanceof THREE.Mesh)  {
     to.geometry.computeBoundingBox();
     if(mode=='v') dy += to.geometry.boundingBox.min.y;
     if(mode=='^') dy += to.geometry.boundingBox.max.y; // Punkt ?ber "to"
  }  // console.log(to.name,"to dy:",dy);

  var obj;
  if(geometry==0)
  {        //// Nur imagin?res objekt
    obj    = new THREE.Object3D;
    meshDY = 0;
  } else  //// Ggf.eues Mash erzeugen, H?he und Winkel-Offset ermitteln
  { if(geometry instanceof THREE.Mesh)
          obj = geometry.clone();  // Ist schon ein Mesh
    else{ obj = new THREE.Mesh( geometry, material );
          obj.name = geometry.name; obj.name+= ">Mash>" };
    if (rx !== undefined)  obj.rotation.x = rx;
    var meshDX = 0; var meshDY = 0; var meshDZ = 0;
    if (geometry!=0) {
    var helper  = new THREE.BoundingBoxHelper(obj,0x888888);
        helper.update(); // console.log('helper:',helper)
        if(mode=='v') dy -= helper.box.max.y;
        if(mode=='^') dy -= helper.box.min.y; // obj-H?he ?ber "to"
    }
  } //console.log(obj.name, "obj dy:",dy);

  var sc = SinCos(angle      ,x)
  var s9 = SinCos(angle+g(90),z)
  obj.position.y = dy; // Neues Seite an Seite zum Anderen
  obj.position.x = dx + sc[0] + s9[0]; 
  obj.position.z = dz + sc[1] + s9[1];
  obj.rotation.order = "YXZ";
  obj.rotation.y = angle;
  to.add(obj);
  return obj;
} // new_mesh_snap ///


// Kreisscheibe plazieren. auch zum Lochstechen (ggf. hohl und halbrund)
function CircleShape(radius,segments,x,y,dr){
  var points = [];
  var max = 360; if(  dr !== undefined) max = 180 + max/segments/2;

  for(var a=0; a<g(max) ; a+= g(max/segments) )
  { var sc = SinCos(a,radius);
    points.push( new THREE.Vector2( x+sc[0],-y+sc[1] ));
  }

  if(  dr !== undefined) // Halbrohr? Innen wieder zur?ck
  {
    for(var a=g(max) ; a>=0 ; a-= g(max/segments) )
    { var sc = SinCos(a,radius-dr);
      points.push( new THREE.Vector2( x+sc[0],-y+sc[1] ));
    }    
  };

  points.reverse() // F?r Scheiben egal, f?r L?cher wichtig
  return new THREE.Shape( points );
}


function SquareShape(u1,v1,u2,v2){
  var points = [];
  points.push( new THREE.Vector2( u2,v1 ));
  points.push( new THREE.Vector2( u2,v2 ));
  points.push( new THREE.Vector2( u1,v2 ));
  points.push( new THREE.Vector2( u1,v1 ));
  return new THREE.Shape( points );
}




function Ring(b,h,d,s) {  // Breite,H?he,Durchmesser,Segmente   rrrrrrrrrr
  var x=h/2; var y=b/2; var r=d/2;
  var points = [];
      points.push( new THREE.Vector3( -y+r, 0, +x+r ));
      points.push( new THREE.Vector3( -y+r, 0, -x+r ));
      points.push( new THREE.Vector3( +y+r, 0, -x+r ));
      points.push( new THREE.Vector3( +y+r, 0, +x+r ));
      points.push( new THREE.Vector3( -y+r, 0, +x+r ));
  var gRing = new THREE.LatheGeometry( points,s );
  var m     = new THREE.Matrix4();
      m.makeRotationX(   g(-90)); gRing.applyMatrix(m); // Von stehend nach Liegend
      m.makeTranslation( 0,-r,0); gRing.applyMatrix(m); // Um halbe H?he nach unten
      return gRing;
}


function Lathe(line,hight,to,index) {  ////////////
   if (index === undefined)  index = 0;
  var points = [];
  for(i=0;i<line.length;i++)
      points.push( new THREE.Vector3( line[i][0], 0, line[i][1] ));
  var e = new THREE.LatheGeometry( points,hight );
  var m = new THREE.Matrix4();
      m.makeRotationX(   g(-90)); // Von Draufsicht nach Seitenansicht
  if(  to === undefined) return e;
  if(!(to instanceof THREE.Geometry))
       to =      new THREE.Geometry();
  to.merge(e,m,index);
  return to;
}




////////////////////////////////////////////////////////////////////////////                        
} ///  Ende der Bauen-Lib  /////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////                        

/*** Notizen: ******
Editor Online:  http://mrdoob.com/projects/htmleditor
 *** 2D Geometry ***
PlaneGeometry(width, height, widthSegments, heightSegments)
Circle (radius, segments, thetaStart, thetaLength)                                         Schiebe ohne Loch
Ring   (innerRadius, outerRadius, thetaSegments, phiSegments, thetaStart, thetaLength)     Scheibe mit  Loch
Shape  (rectShape);                                                                        Beliebige Fl?che(n), auch mit L?schern
 *** 3D Geometry ***
Cube/Box   (width, height, depth, widthSegments, heightSegments, depthSegments)
Cylinder   (radiusTop, radiusBottom, height, radiusSegments, heightSegments, openEnded)
Octahedron (radius, detail)  detail >0 add vertices making it no longer an octahedron.
Sphere     (radius, widthSegments, heightSegments, phiStart, phiLength, thetaStart, thetaLength)
Torus = Donut
IcosahedronGeometry(radius, detail)  Kugel aus wenigen oder vielen Dreiecken (besser als SphereGeometry?)  
 *** XX Geometry ***
Extrude    (shapes, options)                         Fl?che bewegt ?ber Funktion (ggf. Abrundung)
Lathe      (points, segments, phiStart, phiLength)   Linie rotiert->Fl?che/Rohr (Enden offen)
Parametric (func, slices, stacks)                    Quadrahtlanschaft aus Formel
Polyhedron (vertices, faces, radius, detail)         Alles auf einer Kugel: Punkte>Dreiekce>Kugel
Geometry                                             Kann echt alles: Punkte>Dreiekce>Volumen
 *******************/


//  B a u t e i l e:   ////////////////////////////////////////////////////////




function Kuppel(to) {
  var Abstand    = 0.10;
  var HolzDicke  = 0.01;
  var Ringbreite = 0.25;

  var m          = new THREE.Matrix4();
  var geometry   = new THREE.Geometry(); geometry.name = "gRinge"

  function TopRing(Kleiner,Hoehe) {
    var gRing = Ring(Ringbreite,HolzDicke,(HowlR*2+Ringbreite)*Kleiner,44);
      m.makeTranslation(0, Hoehe,0);gRing.applyMatrix(m);
      geometry.merge(gRing);
  }

  TopRing(1.00,Abstand*0);
  TopRing(0.96,Abstand*1);
  TopRing(0.90,Abstand*2); // 64-24=40  40
  TopRing(0.83,Abstand*3);
  TopRing(0.76,Abstand*4);
  m.makeTranslation(0, -(Abstand*4+0.08)/2,0); geometry.applyMatrix(m); // 0-Punkt in die Mitte der Geometry

  var mRinge = new_mesh_snap(geometry, cDark, '^',to,    0,0,0);
      mRinge.position.y += 0.30+4*TopYD; // ?ber der Glasscheibe ist ok, aber h?her bis ?ber die Oberer Holzscheibe!

  var gEnde  = new THREE.CylinderGeometry(HowlR*0.76,HowlR*0.76, 0.03,44); gEnde.name = "gEnde"
               new_mesh_snap(gEnde,   cYellow,'^',mRinge,0,0,0);
}

function Decke() {  // ddd ///////////

  // Decke mit L?chern
  var m = new THREE.Matrix4();
  var sTopTotal = CircleShape( TopR, 88*2,0,0 ); 

      Spin(4,0,0,0, function (angle,step) {
        var sc = SinCos(angle,HowlS);
        sTopTotal.holes.push( CircleShape( HowlR,     88, sc[0],sc[1] ));
      });

  // D?nne Lage wird dreifach genutz
  var gTop = new THREE.ExtrudeGeometry( sTopTotal, {amount: TopYD, bevelEnabled: false } ); gTop.name = "gTop"
      m.makeRotationX( g(-90) ); gTop.applyMatrix(m);
  var gClone = gTop.clone();
      m.makeTranslation(  0,   TopYD+0.15,0);  gTop.merge(gClone,m);
      m.makeTranslation(  0, 2*TopYD+0.30,0);  gTop.merge(gClone,m);
      new_mesh_snap(gTop,  cTop, '^',vDecke,0,0,0);  // Decke ?ber dem Decken-0-Punkt

  // L?cke mit Ringen schlie?en
  var gRing = new THREE.CylinderGeometry( HowlR+0.07, HowlR+0.07, -0.15,  88, 1, true); gRing.name = "gRing"
/*
  var texture2 = THREE.ImageUtils.loadTexture('../pics/streckmetallt.png')  // StreckmetallT.png
      texture2.wrapS = THREE.RepeatWrapping;
      texture2.wrapT = THREE.RepeatWrapping;
      texture2.repeat.set( 36,1 );
*/
  var cStreck = cTop.clone();
//      cStreck.map = texture2;  <<<<<<<<<<<<<<<<<<<<==========================================================================================================
  //r cStreck = new THREE.MeshBasicMaterial( { side: THREE.DoubleSide, map: THREE.ImageUtils.loadTexture( 'streckmetall.png' ) });
      Spin(4,0,0,0, function (angle,step) {
        var x = new_mesh_snap(gRing, cStreck, '^', vDecke, angle,HowlS,0);  x.position.y +=   TopYD;
	      var x = new_mesh_snap(gRing, cTop,    '^', vDecke, angle,HowlS,0);  x.position.y += 2*TopYD+0.15;
      });


  // Glasscheiben in L?cher und Kuppeln dar?ber
  var gHowl = new THREE.CylinderGeometry( HowlR, HowlR, TopYD,  88, 1, false); gHowl.name = "gHowl"
  Spin(4,0,0,0, function (angle,step) {
    howl = new_mesh_snap(gHowl, cGlas, '^', vDecke, angle,HowlS,0)
    if(step>=0) if(go('e')) Kuppel(howl);
  });

    if(params != "") { var gTest = new THREE.SphereGeometry(0.04,0.04); new_mesh_snap(gTest,  cYellow, '.',vDecke,0,0,0); }

} // Decke ///


function Loch(to) { //
  var r1=0.20/2; var r2=0.02; var r3=r2;   var rm = 0.05/2;
  var y1=0.02;   var y2=0.11;  var y3=0.34; var ym = 0.11; var h = 0; //(y1+y2+y3)/2+y1; //  ???????? Halbe H?he, damit Nullpunkt in der Mitte ist, wie bei allen Standard-Geometrien
  var r =0.59;   var yl=0.50;  var sg=16
  var m        = new THREE.Matrix4();
      // Lochwand
  var gLoch    = new THREE.CylinderGeometry( HowlFR, HowlFR, -yl, sg*2 ,1,true); gLoch.name = "gLoch"; // -yl macht die Innenw?nde sichtbar
  var mLoch    = new_mesh_snap(gLoch, cGrey, 'v',to,0,0,0);

      // Lochboden
  var cLoch    = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture('../pics/loch.png' ) });  //
  var gCircle  = new THREE.CircleGeometry( HowlFR, sg*2 );
      m.makeRotationX( g(-90) ); gCircle.applyMatrix(m);
  var mCircle  = new_mesh_snap(gCircle, cLoch, 'v',mLoch,0,0,0);

      // Trichter
  var cTrichter= new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/trichter.png' ), side: THREE.DoubleSide });  //
  var trichter = new THREE.CylinderGeometry( r1, r1, y1, sg ,1,true); // Zylinder unten
  var tric     = new THREE.CylinderGeometry( r2, r1, y2, sg ,1,true); // Trichter
  var rohr     = new THREE.CylinderGeometry( r3, r2, y3, sg ,1,true); // Rohr oben
      m.makeTranslation(0, y1      /2+h,0); trichter.applyMatrix(m);
      m.makeTranslation(0, y1+y2   /2+h,0); trichter.merge(tric, m)
      m.makeTranslation(0, y1+y2+y3/2+h,0); trichter.merge(rohr, m, 1 );

  var xTrichter = new THREE.MeshFaceMaterial( [ cLight, cTrichter ] );  

  var geometry = new THREE.Geometry();
  Spin(5,0,0,0, function (angle) {
    var clone = trichter.clone();
      m.makeTranslation( r,+y1/2-yl,0); clone.applyMatrix(m);       
      m.makeRotationY(   angle-g(90) ); //clone.applyMatrix(m);
      geometry.merge(clone,m);
//      new_mesh_snap(clone, xTrichter, '^', mCircle,angle,0,0);
  });

  var x = new_mesh_snap(geometry, xTrichter, '^', mCircle,0,0,0);
//      console.log(x);

      // Mittelst?tze
  var gStuetze = new THREE.CylinderGeometry( rm, rm, yl-ym, sg ,1,false);
                 new_mesh_snap(gStuetze, cFloor, '^',mCircle,0,0,0);
      // Mittelring
  var gRing = Ring(0.02,0.01,0.24,sg)
  var ri=new_mesh_snap(gRing, cDark, 'v',to,0,0,0);
         new_mesh_snap(gRing, cFloor,'v',ri,0,0,0);
      // Mittelstreben
  var gStab = new THREE.BoxGeometry(0.015,0.015,0.24/2*1.2)
      m.makeRotationX( g(-45) ); gStab.applyMatrix(m);
  Spin(4,0,0,0, function (angle) {
      new_mesh_snap(gStab, cFloor, 'v', ri, angle,0.07,0,0)
  });

}


function Boden(sub) { // bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb

  var sBoden  = CircleShape(         FloorR,  8,0,0 ); // Achteck!
  var sLoch   = CircleShape(         HowlFR, 44,0,0 );

      // 1+5 L?cher im Boden 
      Spin(8,2,6,0, function (angle) {
        var sc = SinCos(angle,HowlFS);
        sBoden.holes.push( CircleShape( HowlFR, 88, sc[0],sc[1] ));    });
      sBoden.holes.push(   CircleShape( HowlFR, 88,     0,    0 ));
      // 2 Aufzugnsl?cher
      Spin(4,0,1,g(-45), function (angle) {
        var sc = SinCos(angle,LiftS);
        sBoden.holes.push( CircleShape( LiftR,  88, sc[0],sc[1] ));    });

  var gBoden = new THREE.ExtrudeGeometry( sBoden, {amount: FloorYD, bevelEnabled: false } ); gBoden.name = "gBoden"
  var m      = new THREE.Matrix4();
      m.makeRotationX( g(-90) ); gBoden.applyMatrix(m);
      m.makeTranslation( 0,-FloorYD/2,0); gBoden.applyMatrix(m); // Um halbe H?he nach unten
  var mBoden = new_mesh_snap(gBoden,  cFloor, 'v',vBoden,0,0,0);  // Decke ?ber dem Decken-0-Punkt

  // Glassscheiben in den Bodenl?chern
  var gHowlF = new THREE.CylinderGeometry( HowlFR, HowlFR, FloorYD, 88, 1, false)
  Spin(8,2,6,0, function (angle) {
    m = new_mesh_snap(gHowlF, cGlas, '.', mBoden, angle,HowlFS,0)
    if(go('e')) Loch(m);
  });

  m = new_mesh_snap(  gHowlF, cGlas, '.', mBoden,     0,     0,0)
  if(go('e')) Loch(m);

  if(params != "") { var gTest = new THREE.SphereGeometry(0.02,0.02); gTest.name = "gTest"; new_mesh_snap(gTest,  cYellow, '.',vBoden,0,0,0);  }

  return mBoden;
} // Boden ///


function Aufzug(){ // aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa


  // Lift-Griffe    (http://threejs.org/examples/webgl_geometry_extrude_shapes.html)
  var GriffDick  = 0.005;
  var GriffBreit = 0.02;
  var GriffUnten = 0.21/2;
  var GriffOben  = 0.13/2;
  var GriffHoch  = 0.05;
  var GriffMitte = GriffOben + GriffHoch * 0.5;  // Etwas steiler als 45?
   
  var points=[new THREE.Vector2(         0           , + GriffUnten            ) //1
             ,new THREE.Vector2( GriffDick           , + GriffUnten            ) //2
             ,new THREE.Vector2( GriffDick           , + GriffMitte            ) //3
             ,new THREE.Vector2( GriffHoch           , + GriffOben             ) //4
             ,new THREE.Vector2( GriffHoch           , - GriffOben             ) //5
             ,new THREE.Vector2( GriffDick           , - GriffMitte            ) //6 
             ,new THREE.Vector2( GriffDick           , -(GriffMitte-GriffDick) ) //7 =
             ,new THREE.Vector2( GriffHoch-GriffDick , -(GriffOben -GriffDick) ) //8
             ,new THREE.Vector2( GriffHoch-GriffDick ,  (GriffOben -GriffDick) ) //9
             ,new THREE.Vector2( GriffDick           ,  (GriffMitte-GriffDick) ) //10=
             ,new THREE.Vector2( GriffDick           , - GriffUnten            ) //11
             ,new THREE.Vector2(         0           , - GriffUnten            ) //12
             ];
 
  var shape  = new THREE.Shape( points );
  var gGriff = new THREE.ExtrudeGeometry( shape, {amount: GriffBreit, bevelEnabled: false } );

  
  // R?hren

  var m     = new THREE.Matrix4();
      /// ConusRing ganz oben, muss einzeln sein, sonst wird die Ecke weich gezeichnet
  var LifRY = 0.03;
  var gLift = Lathe([[LifCR,TopY-LifRY],[LifCR,  TopY]],160,1); // Ring ganz oben
      /// Konus oben
  var o = TopY-LifRY
      Lathe([[LiftR+0.04,o-LifCY],[LifCR     ,o      ]],160,gLift,1); // Konus am Lift oben - Anderes Material!
      Lathe([[LiftR     ,o-LifCY],[LiftR+0.04,o-LifCY]],160,gLift);   // Konus unterseite
      Lathe([[LiftR     ,o],[LiftR     ,o-LifCY      ]],160,gLift);   // Loch im Konus innen (Daher P1 und 2 vertauscht)

      /// Boden des Lifts (im Achteck-Boden) und Pfropfen oben 
  var gLiftB = new THREE.CylinderGeometry(LiftR-0.02  ,LiftR-0.02,  FloorYD,   160, 1, false)
  var gLiftD = gLiftB.clone();
      m.makeTranslation(0,o-LifCY+FloorYD  ,0); gLiftD.applyMatrix(m); gLift.merge(gLiftD)
      m.makeTranslation(0,       -FloorYD/2,0); gLiftB.applyMatrix(m); gLift.merge(gLiftB)
      
      // Lift-halb-Rohr      
  var sLiftP = CircleShape(LiftR+0.04, 160, 0, 0, 0.04);
  var gLiftP = new THREE.ExtrudeGeometry( sLiftP, {amount: TopY, bevelEnabled: false } );
      m.makeRotationX( g(-90) ); gLiftP.applyMatrix(m); // Extrude senkrecht stellen
      m.makeRotationY( g(-90) ); gLiftP.applyMatrix(m); // T?r richtig hin drehen
      gLift.merge(gLiftP)
  var map = THREE.ImageUtils.loadTexture("../pics/lift.png" );
  var cSchraege = cLift.clone();  
      cSchraege.map     = map;
  var xLift = new THREE.MeshFaceMaterial([cLift,cSchraege]);
  var mLift = new THREE.Mesh(gLift,xLift); mLift.name = "mLift"
    
  // Lift-T?r
  var sTuer = CircleShape(LiftR-0.005, 160, 0, 0, 0.01);
  var gTuer = new THREE.ExtrudeGeometry( sTuer, {amount: TopY+0.1, bevelEnabled: false } );
      m.makeRotationX( g(-90) ); gTuer.applyMatrix(m);
      m.makeRotationY( g(-90) ); gTuer.applyMatrix(m);
      m.makeTranslation(0,-(TopY+0.1)/2,0); gTuer.applyMatrix(m);
  var mTuer = new THREE.Mesh(gTuer,cLift); mTuer.name = "mTuer"

  // Wo sind 0 Grad?
  // In WebGL/three ist der Default-Blickwinkel von +Z nach 0. Im Code ist Z=Cosinus(0) =1, also "vorne".
  // Daher ist beim  Default-Blickwinkel, bei der Seitenansicht 0? vorne.
  // Die/Josefs Draufsichten mu? man kippen. Dann wird aus Oben das Hinten bei 180?
  // Und die Aufz?ge sind unten/vorne bei 0?  -/+ 45?
  // Wie ist die Drehrihtung? Beginnend bei 0 Grad vorne/unten geht es weiter mit
  // rechts, hinten/oben, links. Also bei der Draufsicht GEGEN den Uhrzeiger-Sinn!
  var tuer;
  Spin(4,0,1,g(-45), function (angle) {

          var mLix = new_mesh_snap(mLift,     0, '^', vBoden,angle,LiftS,0); mLix.position.y -= FloorYD // Lift-R?hre und Alles, bis IN den Boden
              tuer = new_mesh_snap(mTuer, cLift, 'v', vDecke,angle,LiftS,0); // T?r (R?hre - Halb)

     var g1 = new_mesh_snap(gGriff, cDark, '.', tuer, g(45+ 40) ,LiftR-0.015,0);  g1.rotation.y -= g(-90);  g1.position.y+=(-TopY/2+0.97);
     var g2 = new_mesh_snap(gGriff, cDark, '.', tuer, g(45+  0) ,LiftR-0.015,0);  g2.rotation.y -= g(-90);  g2.position.y+=(-TopY/2+1.26);
     var g3 = new_mesh_snap(gGriff, cDark, '.', tuer, g(45- 90) ,LiftR-0.015,0);  g3.rotation.y -= g(-90);  g3.position.y+=(-TopY/2+1.26);
     var g4 = new_mesh_snap(gGriff, cDark, '.', tuer, g(45-130) ,LiftR-0.015,0);  g4.rotation.y -= g(-90);  g4.position.y+=(-TopY/2+0.97);
         tuer.rotation.y =  g(-44); // -45? = ganz offen
  });//callback&Spin
  tuer.rotation.y =  g(43);
  turn = tuer;
}// Lifts



var ConeType = new Array(
// -/0: Nicht da  K/M/L=1/2/3=5/6/7: 10/15/20cm hoch 
//  S  +4:  plus gelbem Streck-Metal
//  B +10:  plus beleuchtet  -- Meisst ist LSB zusammen: 3+4+8=17
//  "Hinterleuchet", da k?nnte man eine Lichtquelle plazieren.Todo
//  "Durchgesteckt" wird erst mal nicht realisiert

/* A  */  17,  /*  < Links <                                    Gerade raus                                    > Rechts >
/*    *  KxA KxB7A  B  C  D   B8A  B  C  D  E!   9A  B  C  D    0A  B  C  D    1A  B  C  D    2A  B  C  D  E!   3A  B  C  D  */
/* K0 */   2,    3, 3, 0, 0,    3, 1, 3, 2,17,    3, 3,17, 3,    2, 3, 1, 1,     1, 2, 3, 3,   3,17, 2,17,11,    2, 3,3*0,0, /* vorne     0 Grad */
/* K1 */   3,   17, 3,3-3,1-1,  1, 3,17, 2,       3, 3, 3,13,    3,17, 3, 3,     1, 2, 3,17,   7,17, 2, 1,       2, 2,3-3,1, /* rechts>  90 Grad */
/* K2 */   3,    1,17, 1, 1,   17, 2, 3, 1, 3,    3, 1, 2, 3,    3,17, 2, 3,    17, 1, 2, 7,   7,17, 1,17,12,    2, 3, 0,17, /* hinten  180 Grad */
/* K3 */   3,    3, 0, 0, 3,    2,17, 1, 3,       3, 3, 2, 1,   17, 3, 1, 3,     3, 3,17, 3,   3, 1,17, 2,      17, 3, 0, 0, /* <links  270 Grad */

/*    *  XxA  B  C  D  E  F    // Gegen den Uhrzeiger-Sinn!
/* X0 */  17, 2,17, 3, 1, 0,   //  45 Grad: Vorne/unten-Rechts
/* X1 */  17, 1,17, 3, 0, 3,   // 135 Grad: Rechts-Hinten/oben
/* X2 */  17, 2,17, 3, 0, 3,   // 225 Grad: Hinten/oben-Links
/* X3 */  17, 3, 3, 3, 0, 0,   // 315 Grad: Links-Vorne/unten
/*    */ -1); // Array-Ende

function Cone(to,angle,x,z,name) { // ccccccccccccccccccc ////////////

  var gCone2B = new THREE.SphereGeometry(   0.060/2                          );
  var gCone2H = new THREE.CylinderGeometry( 0.105/2, 0.105/2,   0.199, 32, 1, true ); // Openended
  
  var gCone20 = new THREE.CylinderGeometry( ConeRo,  0.12 /2,    0.20, 32 );
  var gCone15 = new THREE.CylinderGeometry( ConeRo,  0.135/2,    0.15, 32 );
  var gCone10 = new THREE.CylinderGeometry( ConeRo,  0.15 /2,    0.10, 32 );  
  var gCirc   = new THREE.CircleGeometry(   ConeRo,                    32 );  // gekippt

  var con;
  var type = ConeType[conen]; // console.log(name,conen,type)
  var bel  =  false; if(type>10) {type-=10; bel = true; gsm=true;}; // beleuchtet?
  var gsm  =  false; if(type> 4) {type-= 4; gsm = true}; // gelbes StreckMetal?  
  var gCone=gCone10; if(type==2) gCone=gCone15; if(type>=3)gCone=gCone20
  var cStreck = new THREE.MeshBasicMaterial( { side: THREE.DoubleSide, map: THREE.ImageUtils.loadTexture( '../pics/streckmetall.png' ) });

  if(type>0) {
    mCone =      new_mesh_snap(gCone,   cCone,   'v', to, angle,x,z); mCone.name = name; // Cone. Der bekommt ggf. noch was dazu:
    if(     gsm) new_mesh_snap(gCone2H, cStreck, 'v', to, angle,x,z); // Gelbe beleuchtete Seule im Cone
         if(bel) new_mesh_snap(gCone2B, cLight,  'v', to, angle,x,z); // Beleuchtet
  
    mCirc =      new_mesh_snap(gCirc,   cLight,  'v', to, angle,x,z); // Schummel: Statt der L?cher, die Minuten dauern w?rden hier helle Flecken:
    mCirc.rotation.x = g(90)
    mCirc.position.y-= (0.0002-ConeRo); //Etwas unter der Decke, damit die Fl?che sich nicht mit der Decke vermischt in der Grafigkarte
  }
  conen++;
  return mCone;
} // Todo: L?cher, H?hen, Gelbzylinder mit Licht.    Unklar Decken-PDF Rechts-Unten: Im Lift sind Lampen mit LLLMKD markiert? 

function Cones(){

  Cone(vDecke,0,0,0,"A");  // Der in er Mitte  --  Parameter: parent(pos),axis(angle),radius(from_parent)

  // Null Grad (K) "Kreuz"
  Spin(4,0,3,0, function (angle,stepM) { // K0 bis 3
    Cone(vDecke,angle,0.45,0,"K"+stepM+"A");

    // "Strahlen" um die L?scher
    var howl = new_mesh_snap(0, cGlas, '^', vDecke, angle,HowlS,0)  // (KnB)

    var d = 0.33
    Spin(15.5,-3,+3,0, function (angle,step) {  // KnB7 bis 3
      if(step<0)  var name = "K"+stepM+"B"+(step+10);
      else        var name = "K"+stepM+"B"+ step;
                  Cone(howl,angle,HowlR+1*d,0,name+"A"); // KnBnA
                  Cone(howl,angle,HowlR+2*d,0,name+"B");
                  Cone(howl,angle,HowlR+3*d,0,name+"C");
      var coneD = Cone(howl,angle,HowlR+4*d,0,name+"D");
//                Cone(howl,angle,HowlR+5.5*d,0);  // W?hre Identisch mit Unten K3B6+ und

      if(  (stepM%2==0) && (Math.abs(step)==2))  // Die Vier vereckten: Nur Oben/Unten und bei etwa 45?
      {
        vDecke.updateMatrixWorld();
        var world = howl.localToWorld( new THREE.Vector3( coneD.position.x, coneD.position.y, coneD.position.z ) );
        var z = d; if(world.z<0) z = -d;
        var x = d; if(world.x<0) x = -d;
        Cone(vDecke,0,world.z+z/2,world.x+x,name+"E");  // x,z von cone=new_mesh_snap sind relativ zur Drehung, daher hier x=z und y=x

      }

    });// "Strahlen"
    
  });

  // 45 Grad    Die zwischen den L?schern
  Spin(4,0,0,g(45), function (angle,step) {
    var o=0.35; var 
    d =1.0;  Cone(vDecke,angle,  d,0  ,"X"+step+"A");
    d+=0.5;  Cone(vDecke,angle,  d,0  ,"X"+step+"B");
    d+=0.4;  Cone(vDecke,angle,  d,0.2,"X"+step+"C"); // C<<
             Cone(vDecke,angle,  d,-.2,"X"+step+"D"); // >>D
    d+=0.3;  Cone(vDecke,angle,  d,0  ,"X"+step+"E");
             Cone(vDecke,angle,3.6,0  ,"X"+step+"F");
  });
  
  // console.log('Anzahl der Konen:',conen,ConeType[conen]);
}// CONES

function Lamellen(){ // llll
  // Lamellenform:
  var KurveI = [ [0,0],[166,0],[206,0],[206,99],[197,96],[185,94],[171,92],[155,90],[131,88],[104,87],[76,87],[70,89],[66,93],[65,99],[66,105],[68,110],[58,107],[47,104],[30,102],[15,103],[0,108] ];
  var KurveA = [ [0,0],[166,0],[206,0],[206,90],[197,89],[185,87],[171,85],[155,83],[131,81],[104,80],[72,80],[66,84],[62,91],[61,99],[65,106],[68,110],[58,105],[47,101],[30, 99],[15, 99],[0,102] ];
  var z = 0.03

  var points = []; 
  for(var i=1; i<KurveA.length-11; i++) {
      var x  = KurveA[i][0] / 206 * 0.7;  // durch Maximalwert gibt Normierung auf 1, dann auf Realmass
      var y  = KurveA[i][1] / 206 * 0.7;
    points.push( new THREE.Vector2( x, y ));
  }
  var shape = new THREE.Shape( points );
  var gCooM = new THREE.ExtrudeGeometry( shape, {amount: z, bevelEnabled: false,steps:2 } );  // CoolMinus f?r ?ber Monitore
  // Spitze herausschieben 
  for(var i = 0; i<8; i++)
  {
        gCooM.vertices[ 9+i].x = KurveI[ 9-i][0] / 206 * 0.7;
        gCooM.vertices[ 9+i].y = KurveI[ 9-i][1] / 206 * 0.7;
  }

  var points = []; 
  for(var i=0; i<KurveA.length; i++) {
      var x  = KurveA[i][0] / 206 * 0.7;  // durch Maximalwert gibt Normierung auf 1, dann auf Realmass
      var y  = KurveA[i][1] / 206 * 0.7;
    points.push( new THREE.Vector2( x, y ));
  }
  var shape = new THREE.Shape( points );
  var gCool = new THREE.ExtrudeGeometry( shape, {amount: z, bevelEnabled: false,steps:2 } );
  // Spitze herausschieben 
  for(var i = 0; i<19; i++)
  { gCool.vertices[41-i].x = KurveI[i][0] / 206 * 0.7;
    gCool.vertices[41-i].y = KurveI[i][1] / 206 * 0.7;
  }
  gCool.vertices[61-14].z = z/2;
  gCool.vertices[19-14].z = z/2;

  var m     = new THREE.Matrix4();
      m.makeTranslation(0,0,-z);            gCool.applyMatrix(m); gCooM.applyMatrix(m);
  // Pyramiden dazu  
  var gPyramid = new THREE.CylinderGeometry(0, z*0.8, z*0.3, 4, true);
  var gClone   = gPyramid.clone();
      m.makeRotationX( g(-90)         );    gClone.applyMatrix(m);
      m.makeTranslation(0.49,0.19,-z*1.18); gClone.applyMatrix(m);
      gCool.merge(gClone);
  var gClone   = gPyramid.clone();
      m.makeRotationX( g(+90)         );    gClone.applyMatrix(m);
      m.makeTranslation(0.49,0.19,+z*0.18); gClone.applyMatrix(m);
      gCool.merge(gClone);
  // Rauten an die Spitzen
      m.makeScale ( 5, .7, .7 );          gPyramid.applyMatrix(m);
  var gClone   = gPyramid.clone();
      m.makeRotationX( g(+90)         );    gClone.applyMatrix(m);
      m.makeRotationZ( g(+30)         );    gClone.applyMatrix(m);
      m.makeTranslation(0.11,0.295,+z*0.12);gClone.applyMatrix(m);
      gCool.merge(gClone);
  var gClone   = gPyramid.clone();
      m.makeRotationX( g(-90)         );    gClone.applyMatrix(m);
      m.makeRotationZ( g(+30)         );    gClone.applyMatrix(m);
      m.makeTranslation(0.11,0.295,-z*1.12);gClone.applyMatrix(m);
      gCool.merge(gClone);

  // Lamelle zurechtdrehen:
      m.makeRotationY(    g( -90)       ); gCool.applyMatrix(m);  gCooM.applyMatrix(m);
      m.makeRotationX(    g(-150)       ); gCool.applyMatrix(m);  gCooM.applyMatrix(m);
  // Zweite Lamelle hinter&unter die Erste  
  var gClone = gCool.clone();                                 var gCoo2 = gCool.clone();
      m.makeTranslation(0,-0.28,0.655);    gClone.applyMatrix(m); gCooM.applyMatrix(m);
      gCool.merge(gClone);                                        gCoo2.merge(gCooM);

  // Viele im Kreis
  var gCoolsA = new THREE.Geometry(); // Alle 3 mal (Alles in einem schaft iPad2 nicht)
  var gCoolsB = new THREE.Geometry(); // Beschnitten vor Monitor
  var weg = [103,104,105 , 109,110,111 , 115,116,117,118];
  Spin(220,0,220/4,0, function (angle,step) {  // 110 55
                             gCloneA = gCool.clone();
      if(contains(weg,step+55+10)) gCloneB = gCoo2.clone();
      else                      gCloneB = gCool.clone();
      m.makeTranslation(0,0,TopR+0.77); gCloneA.applyMatrix(m); gCloneB.applyMatrix(m);
      m.makeRotationY(       angle   ); gCloneA.applyMatrix(m); gCloneB.applyMatrix(m);
      gCoolsA.merge(gCloneA);
      gCoolsB.merge(gCloneB);
  });
  var l10 = g(360*10/220)
  Spin(4,0,0,l10, function (angle,step) {  // 10 Lamellen nach Links verdrehen, damit alle Beschnittenen in einem Viertel sind
    var gCools = gCoolsA; if(step==1) gCools = gCoolsB;
    mL = new_mesh_snap(gCools, cLamellen, 'v', vDecke, angle,0,0); mL.position.y += 0.3  // Etwas h?her als unter der Decke
  });

  // Blende hinter den Cools
  var points = [];
      points.push( new THREE.Vector3(TopR+TopK*AchtA,0,-0.00)); // Aussen = Deckenradius + Kasten * Achteckfaktor f?r die Ecken
      points.push( new THREE.Vector3(TopR+TopK*AchtA,0, 0.85)); //   Oben
      points.push( new THREE.Vector3(TopR,           0, 0.85)); // Innen: Spitzen = Decken-Kreis
      gHinter = new THREE.LatheGeometry( points,8 ); //  ACHTECK!   (Lathe will Vector3)
  var x = new_mesh_snap(gHinter, cBlack, '.', mL,-l10,0,0,g(-90)); x.position.y -= 0.4  // Etwas tiefer als die Lamellen

}// Cools



//?! Da die THREE-Box beim Merge mit mehreren Farben Error in three.js kommt, hier ein eigener W?rfel:  !?//
function Box(a, b, c) { 
    var g = new THREE.Geometry();
    var v1 = new THREE.Vector3( a /2,  b /2,  c /2);    var v2 = new THREE.Vector3( a /2,  b /2, -c /2);
    var v3 = new THREE.Vector3( a /2, -b /2,  c /2);    var v4 = new THREE.Vector3( a /2, -b /2, -c /2);
    var v5 = new THREE.Vector3(-a /2,  b /2, -c /2);    var v6 = new THREE.Vector3(-a /2,  b /2,  c /2);
    var v7 = new THREE.Vector3(-a /2, -b /2, -c /2);    var v8 = new THREE.Vector3(-a /2, -b /2,  c /2);
    g.vertices.push(v1, v2, v3, v4, v5, v6, v7, v8);
    g.faces.push(new THREE.Face3(0, 2, 1));    g.faces.push(new THREE.Face3(2, 3, 1));
    g.faces.push(new THREE.Face3(4, 6, 5));    g.faces.push(new THREE.Face3(6, 7, 5));
    g.faces.push(new THREE.Face3(4, 5, 1));    g.faces.push(new THREE.Face3(5, 0, 1));
    g.faces.push(new THREE.Face3(7, 6, 2));    g.faces.push(new THREE.Face3(6, 3, 2));
    g.faces.push(new THREE.Face3(5, 7, 0));    g.faces.push(new THREE.Face3(7, 2, 0));
    g.faces.push(new THREE.Face3(1, 3, 4));    g.faces.push(new THREE.Face3(3, 6, 4));     
    return g;
}    


function Stachelei() { // kkkkkkkkkkkkkkkkkkkkk

  var hoehe  = 2.1; // [m]      297p Erst mal wie Telenoseei laut JH    1.79?
  var ho     = hoehe * 0.56;  // 164p H?henanteil oben 56%
  var hu     = hoehe-ho;      // 133p Der Rest ist unten
  var breite = hoehe * 0.7;   // 236p Breite/Durchmesser ?ber 8-Eck aussen. 236p/297p=0,7946 => 80% Breitenfaktor  0.80;    
  var o      = hoehe * 0.20;  // 295p x 338p Elipse oben ist spitzig: 295-236=59/2=30 Mittelpunkt-Versatz (0,1986531986532)
  var s      = 0.1;           // Stachelstreifenbreite
  // Die Elipse oben ist breiter als das Ei. Es wird nur der Aussenteil genutzt.
  // Der Sinus-Anteil des Radius ist also gr??er, danach mu? der Teil wieder abgezogen werden.
  
  var all      = new THREE.Geometry();
  var segment  = new THREE.Geometry();
  var streifen = new THREE.Geometry();
  var m        = new THREE.Matrix4();
  
  var  seg = 8;       // Segment-/Seitenzahl
  var aSeg = 360/seg; // Grad pro Seite
  
  segment.vertices.push(  new THREE.Vector3(    0, ho-0.10, 0   ) // Oben anfangen
                        , new THREE.Vector3(    0, ho-0.10, 0   ) ); 
  streifen.vertices.push( new THREE.Vector3( +s/2, ho-0.12, 0.1 )
                        , new THREE.Vector3( -s/2, ho-0.12, 0.1 ) ); 

function EiSegment(a,q,r,aussen)  // Ermittelt eine Position im Raum, abh?ngig von der Winkelh?he a [in 0..180 Grad]
                                  // und dem Abstand von der Mitte [q] oder dessen Maximum
{                                 // und dem Zusatzabstand von der Ebene [r].  Das Stachelband ist "aussen" = true
  if(r===undefined) var r = 0;
  var sin = Math.sin(g(a));
  var cos = Math.cos(g(a));
  var tan = Math.tan(g(180/seg));

  // 2. Parameter q [in Metern] ist die Breite: Ist die +/-1  wird die Kante der Fl?che errechtet
  var b=breite/2*tan;

  var ros = (breite/2+o+r)*sin-o;  // Elipsen-radius, variiert um offset
  var bos = ros*tan;

  // 295p x 338p Elipse oben  ist spitzig: 295-236=59/2=30 Mittelpunkt-Versatz
  // 236p x 253p Elipse unten hat gleiche Breite 253/2=121.5p etwa 133p
  if(a<90) // Oben bis Mitte
  { var x = bos   * q;  // Breite des Streifen
    var y = (ho+r)*cos;   // H?he
    var z = ros;      // Dicke des Eis
  }else{  // Mitte bis Unten
    var x =  b    *sin * q;
    var y = (hu+r)*cos;
    var z = (breite/2+r)*sin;
  }

  if(Math.abs(q)!=1) {  x = q;         };
  if(aussen     ==1) {  z = z * AchtA; };

  return new THREE.Vector3(x, y, z);
}


  var steps = 46; // 12*2+1;
  var step  = 180/steps;
  var stepO = 0;
  var wNadel= 1.0;  //Grad
  var zNadel= s/18;
  var hNadel=0.005;
  var i=0; var j=0;
  for( w=35;w<=180-15;w+=(step+stepO) )  // Von Oben 0 bis Unten 180 Grad
  {
    stepO = Math.sin(g(w))*1.0; // Hoehe/Breite;
		// helle grumme Seiten
    segment.vertices.push( EiSegment(w,+1), EiSegment(w,-1) );
    segment.faces.push(  new THREE.Face3( i+0, i+1, i+2 ) );
    segment.faces.push(  new THREE.Face3( i+1, i+3, i+2 ) );
		// Stachelleiste                                                                                               0     1
    streifen.vertices.push(                         EiSegment(w-step/2       ,      0,  0.06,1),  // Mitte j=2        6
      EiSegment(w-step/2       ,+zNadel,hNadel,1),  EiSegment(w-step/2       ,-zNadel,hNadel,1),  //    l,r =3,4     324
      EiSegment(w-step/2+wNadel,      0,hNadel,1),  EiSegment(w-step/2-wNadel,      0,hNadel,1),  //    u,o =5,6      5
      EiSegment(w              ,    s/2,0     ,1),  EiSegment(w              ,   -s/2,     0,1)   //    R,L =7,8   7     8
      );
    // Nadelstreifen-Segent mit Nadel und Viereck unten:
    streifen.faces.push( new THREE.Face3( j+0, j+1, j+6 ) );    streifen.faces.push( new THREE.Face3( j+1, j+8, j+4 ) );
    streifen.faces.push( new THREE.Face3( j+5, j+8, j+7 ) );    streifen.faces.push( new THREE.Face3( j+3, j+7, j+0 ) );
    streifen.faces.push( new THREE.Face3( j+1, j+4, j+6 ) );    streifen.faces.push( new THREE.Face3( j+8, j+5, j+4 ) );
    streifen.faces.push( new THREE.Face3( j+7, j+3, j+5 ) );    streifen.faces.push( new THREE.Face3( j+0, j+6, j+3 ) );
    streifen.faces.push( new THREE.Face3( j+2, j+6, j+4 ) );    streifen.faces.push( new THREE.Face3( j+2, j+4, j+5 ) );
    streifen.faces.push( new THREE.Face3( j+2, j+5, j+3 ) );    streifen.faces.push( new THREE.Face3( j+2, j+3, j+6 ) );
    i+=2; j+=7;
  }

  for(r=0;r<g(360);r+=g(aSeg))  // alle Ei-Seiten und -Streifen
  { m.makeRotationY(r             );  all.merge(segment, m);
    m.makeRotationY(r + g(aSeg/2) );  all.merge(streifen,m,2);
  }

  var cDisco   = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/disco.jpg' ) });
//var cEiAn    = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/lampeein.png' ) });

	var gBox = Box(0.6,0.04,0.02);  // Stange im Ei
  var v =	EiSegment(93., 0 ,-0.02);
  m.makeTranslation( v.x, v.y, -v.z ); all.merge(gBox,m,4);
  
      gBox = Box(0.04,0.2,0.02);  // Stange senkrecht
      v = EiSegment(87., 0 ,-0.02);
  m.makeTranslation( v.x, v.y, -v.z ); all.merge(gBox,m,4);
  
      gBox = Box(0.06,1.0,0.06);  // Stange lang
  m.makeTranslation( 0, -0.3, 0 ); all.merge(gBox,m,3);
  
	    gBox = Box(0.12,0.01,0.3);  // Brett, das aus Ei heraus ragt (duch das Loch)
  var v =	EiSegment(85, 0 ,0);
  m.makeTranslation( v.x, v.y+0.045, -v.z ); all.merge(gBox,m,3);
	    gBox = Box(0.15,0.05,0.15); // "Tastatur"
  var v =	EiSegment(85, 0 ,0);
  m.makeTranslation( v.x, v.y+0.075, -v.z-0.1 ); all.merge(gBox,m,4);
	    gBox = new THREE.CircleGeometry(0.09, 32)  // "Disco-Loch"
  var v =	EiSegment(80, 0 ,+0.001);
  m.makeRotationX(g(3*90-85)); gBox.applyMatrix(m);
  m.makeTranslation( v.x, v.y, -v.z ); all.merge(gBox,m,5); // Auf Seite setzen

											                 //   0       1       2   an3   aus4, 5       6
//var xEi = new THREE.MeshFaceMaterial( [ cEi, cEiStr, cMetal,cEiAn,cBlack, cDisco, cYellow ] );  
	
// Hilfsfuntion f?r die Applikationen, zb. Halbkugeln, auf dem Stackelei
function EiApp(Senkrecht,Waagerecht,Seite,Type) // Senkrecht: Position etwa in Meter ,Waagerecht: Abstand von Seitenmitte in Metern, 
      // Seite:0 ist Richtung Raummitte,1 rechts daneben,7 links daneben, 
      // Type: 1/2/3 sind kleine/mittler/gr??er Kugel; <0 bedeutet Durchsichtig;  "Laustsprecher"  .. Stachel  Bananenstecker  Lampe   Schalter  Innenstange  5staner  "Ausenspiegel"
{
  var w = 90-Senkrecht*50;  // ca. 10cm wird zu 5 Grad + 90 f?r die Mitte als 0-Wert
  var v = EiSegment(w, Waagerecht ,0);
  var a = g( 180 + aSeg*Seite );

  var t = Math.abs(Type);
  switch(t)
  {

  case 1:  case 2:  case 3:  // Halbkugel klein/mittel/gross
    var Radi = [0, 0.012 , 0.014 , 0.025]
    var Radius = Radi[t];
    var Farbindex = 2; if(Type<0) Farbindex = 0;
    var Halbkugel = new THREE.SphereGeometry( Radius, 16,16, g(90-w),g(180) ); 
    m.makeRotationZ(    g(90)          ); Halbkugel.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    m.makeTranslation(  v.x, v.y, v.z  ); Halbkugel.applyMatrix(m);  // Positionieren
    m.makeRotationY(a); all.merge(Halbkugel,m,Farbindex); // Auf Seite setzen
    break;

  case 5:  case 6: // Lampe An/Aus
    var d = 0.015; var h = 0.01;
        v = EiSegment(w, Waagerecht ,h/2);
    var gDing = new THREE.CylinderGeometry( d, d, h, 16, 1, true ); 
    var c = 3; if(t>5) c=4;
    m.makeRotationX(    g(w)           ); gDing.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    m.makeTranslation(  v.x, v.y, v.z  ); gDing.applyMatrix(m);  // Positionieren
    m.makeRotationY(a); all.merge(gDing,m,2); // Auf Seite setzen

        v = EiSegment(w, Waagerecht ,h);
        gDing = new THREE.CircleGeometry( d, 16 );
//      gDing = new THREE.PlaneGeometry( d*2,d*2 );
    m.makeRotationX(    g(w-90)         ); gDing.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    m.makeTranslation(  v.x, v.y, v.z  ); gDing.applyMatrix(m);  // Positionieren
    m.makeRotationY(a); all.merge(gDing,m,c); // Auf Seite setzen
    break;//


  case 8: // Puschel-Stecker ausw?rz
    var d = 0.005; var h = 0.04;
    var gDing = new THREE.CylinderGeometry( d, d, h, 8, 1, false ); 
    m.makeRotationX(    g(w)        ); gDing.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    var v = EiSegment(w, Waagerecht ,h/2);
    m.makeTranslation(  v.x, v.y, v.z  ); gDing.applyMatrix(m);  // Positionieren
    m.makeRotationY(a); all.merge(gDing,m,4); // Auf Seite setzen
    break;

  case 10:  // Halbkugel sehr gross gelb
    var Radius = 0.06;
    var Halbkugel = new THREE.SphereGeometry( Radius, 16,16, g(90-w),g(180) ); 
    m.makeRotationZ(    g(90)          ); Halbkugel.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    m.makeTranslation(  v.x, v.y, v.z  ); Halbkugel.applyMatrix(m);  // Positionieren
    m.makeRotationY(a); all.merge(Halbkugel,m,6); // Auf Seite setzen
    break;

  case 11:  // Halb-Elybse sehr gross gelb 
    var Radius = 0.09;
    var Halbkugel = new THREE.SphereGeometry( Radius, 16,16, g(90),g(180) );
    m.makeScale(    0.45,1,1           ); Halbkugel.applyMatrix(m);  // Aus Kugel flache Elypse
    m.makeRotationY(    g(-90)          ); Halbkugel.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    m.makeRotationX(    g(w-90)          ); Halbkugel.applyMatrix(m);  // Von Richtung Oben nach Forne drehen
    m.makeTranslation(  v.x, v.y, v.z  ); Halbkugel.applyMatrix(m);  // Positionieren
    m.makeRotationY(a); all.merge(Halbkugel,m,6); // Auf Seite setzen
    break;

  }
  
}


  // --- Seite 1 halb rechts ---
  EiApp( 0.85 , 0.   , 1,  3);
  EiApp( 0.85 , 0.   , 1,  10);
  EiApp( -0.05,-0.16 , 1,  11);
  EiApp( -0.35, 0.03 , 1,  11);
  EiApp( -0.53,-0.11 , 1,  11);

  EiApp( -0.8 , 0    , 1,   3);
  EiApp( -0.3 ,-0.16 , 1,   3);
  EiApp( -0.18,-0.06 , 1,   1);
  EiApp(  0.06,-0.06 , 1,   1);
  EiApp(  0.07,-0.00 , 1,   1);
  EiApp(  0.07, 0.06 , 1,   1);
  EiApp(  0.07, 0.12 , 1,   1);

  EiApp(  0.00, 0.0  , 1,   8);
  EiApp( -0.06, 0.0  , 1,   8);


  // --- Seite 0 forne ---

  EiApp( 0.485 ,-0.11    , 0, 8); // Stecker oben
  EiApp( 0.435 ,-0.12    , 0, 8);
  EiApp( 0.38  ,-0.13    , 0, 8);
  EiApp( 0.33  ,-0.14    , 0, 8);

  EiApp( 0.28  ,0.18    , 0, 8);
  EiApp( 0.28  ,0.14    , 0, 8);
  EiApp( 0.24  ,0.14    , 0, 8);
  EiApp( 0.24  ,0.18    , 0, 8);

  EiApp( 0.02  ,0.23    , 0, 8);
  EiApp( 0.02  ,0.17    , 0, 8);
  EiApp( 0.02  ,0.12    , 0, 8);


  EiApp( 0.50 ,-0.155 , 0, 5); // Lampen oben links
  EiApp( 0.445,-0.165 , 0, 5);
  EiApp( 0.385,-0.175 , 0, 6);
  EiApp( 0.33 ,-0.185 , 0, 5);
  
  EiApp( 0.07 , 0.12  , 0, 5); // Lampen Mitte rechts
  EiApp( 0.07 , 0.17  , 0, 6);
  EiApp( 0.07 , 0.225 , 0, 5);

  // Untre Lampengruppe, 4 Reihen mit 2,4,3,4 Lampen
  EiApp(-0.16 ,-0.105  , 0, 5);
  EiApp(-0.17 ,-0.18   , 0, 5);

  EiApp(-0.23 , 0.04   , 0, 6);
  EiApp(-0.23 ,-0.055  , 0, 5);
  EiApp(-0.24 ,-0.135  , 0, 5);
  EiApp(-0.23 ,-0.215  , 0, 5);

  EiApp(-0.3 , 0.003   , 0, 5);
  EiApp(-0.3 ,-0.09    , 0, 6);
  EiApp(-0.32,-0.17    , 0, 5);

  EiApp(-0.38 , 0.05   , 0, 5);
  EiApp(-0.38 ,-0.045  , 0, 5);
  EiApp(-0.38 ,-0.13   , 0, 5);
  EiApp(-0.389,-0.20   , 0, 5);


  var py=0.52; var px=0.11
  var dy=0.07; var dx=-0.003;
  //     s  y,  w   x, s, t
  EiApp(py, px, 0, 1); py+=dy; px+=dx;
  EiApp(py, px, 0, 1); py+=dy; px+=(dx*3);
  EiApp(py, px, 0, 1); py+=dy; px+=dx;
  EiApp(py, px, 0, 1); py+=dy; px+=(dx*3);
  EiApp(py, px, 0, 1); py+=dy; px+=(dx*10.5);
  EiApp(py, px, 0, 1);

  EiApp(0.93, -0.02, 0, 1);  // Ganz oben

  EiApp(0.46, -0.135, 0, 1); // die 4 links oben
  EiApp(0.55, -0.13 , 0, 1);
  EiApp(0.63, -0.115, 0, 1);
  EiApp(0.71, -0.1  , 0, 1);

  EiApp(0.69,  0.02   , 0,-3);
  EiApp(0.46,  0.04   , 0,-3);
  EiApp(0.57, -0.09   , 0,-3);
  EiApp(0.56,  0.03   , 0,-3);

  EiApp(0.46, -0.04 , 0, 3);  // Unter Spiegel
  EiApp(0.42,  0.11 , 0, 3);   // reichts darunter

  EiApp(0.39, -0.01   , 0,-3);

  EiApp(0.27, -0.19 , 0, 2); // ?ber Kreuz
  EiApp(0.29, -0.09 , 0, 2);
  EiApp(0.33,  0.0  , 0, 2);
  EiApp(0.29,  0.09 , 0, 2);

  EiApp(0.185,0.16 , 0, -2);  //   ganz rechts davon
  EiApp(0.10 ,-0.21 , 0, 2);  // Links-Kreuz
  EiApp(0.11 ,-0.11 , 0, 2);
  EiApp(0.185,-0.16 , 0, 3);
  EiApp(0.03 ,-0.16 , 0, 3);

  EiApp(-0.0  , 0.022, 0,  1);
  EiApp(-0.0  ,-0.022, 0,  1);
  EiApp(-0.05 , 0	   , 0, -3); // MITTE
  EiApp(-0.05 ,-0.09 , 0, -3);
  EiApp(-0.07 ,-0.20 , 0, -3);

  EiApp(-0.22 , 0.13 , 0,  3);
  EiApp(-0.36 , 0.13 , 0, -3);

  EiApp(-0.46 , 0    , 0, -3);
  EiApp(-0.55 ,-0.17 , 0, -3);
  EiApp(-0.66 , 0.11 , 0, -3);


  m.makeTranslation(0,hu,0); all.applyMatrix(m);

  var r = 0.3;
  var gKopf = new THREE.SphereGeometry( r, 32,32, 0,g(360) , 0,g(30) ); 
  m.makeTranslation(0,hu+ho-r-0.095,0);   all.merge(gKopf,m,1);
  var gFuss = new THREE.CylinderGeometry(0.35,0.43,0.22,8*4);  // Kegelstumpf unten 2*58,72x37p => 0.7/0.87*0.22
  m.makeTranslation(0,0,0);       all.merge(gFuss,m,1);

  all.computeFaceNormals();   // Fl?chenfarbe einrichten
  all.computeVertexNormals(); // Farb?berg?nge weich
  all.mergeVertices()         // Nach V.Normals ist das ok :-)
											                 //   0       1       2   an3   aus4  5      6
  var xEi = new THREE.MeshFaceMaterial( [ cEi, cEiStr, cMetal,cEiAn,cBlack,cDisco,cYellow ] );  
      mEi = new_mesh_snap(all,  xEi, '^', vBoden, 0, 3.6, 0 ); // 0.2
   // mEi.visible = false;
      return mEi;
}



function Komputer() { // kkkkkkkkkkkkkkkkkkkkk2
  var EiSegmente = 24;
  var EiKurve = /*  // |y|  -x-  0,0 ist impliziert im code */
      [ [  7, 58],[ 12,115],[ 65,215],[ 125,296],[ 200,364],[ 281,416],[ 381,455],[ 478,473],[ 567,478],[ 666,472],
        [748,456],[873,414],[968,365],[1048,312],[1111,259],[1170,200],[1215,146],[1256, 81],[1270, 41],[1273,  0] ];
  var gKomputer = new THREE.Geometry(); // "KannAlles"-Geometry braucht Vector3
  var v = -2;
  for(var s=0; s<(EiSegmente); s++) {
    gKomputer.vertices.push( new THREE.Vector3(0,0,0), new THREE.Vector3(0,0,0)); v+=2; // vertices 0 und 1: Die unteren Punkte des Kugel-Segments sind beide 0,0
    var a1 = g(360)/EiSegmente*(s+0.5);   // Winkel der Rechten/Linken Kante
    var a2 = g(360)/EiSegmente*(s-0.5);
    for(var i=0; i<EiKurve.length; i++) {
      var y  = EiKurve[i][0] / 1273;  // durch Maximalwert gibt Normierung auf 1
      var x  = EiKurve[i][1] / 478/2;
      var sc1= SinCos(a1,x);
      var sc2= SinCos(a2,x);
    gKomputer.vertices.push( new THREE.Vector3(sc1[0],y,sc1[1]));
    gKomputer.vertices.push( new THREE.Vector3(sc2[0],y,sc2[1]));

    gKomputer.faces.push( new THREE.Face3( v+0, v+2, v+1 ) );
    gKomputer.faces.push( new THREE.Face3( v+1, v+2, v+3 ) );
    v+=2; // console.log("face:",i,EiKurve.length)
    }  // i
  } // s

  //r gScheibe = new THREE.BoxGeometry(0.1, 0.01, 0.1)
  var gScheibe =           Box        (0.1, 0.01, 0.1)
  var gStange  = new THREE.CylinderGeometry(0.015, 0.015, TopY, 16, 1, false)
  
  var m = new THREE.Matrix4();
  m.makeScale(     1.34,1.79,1.34);  gKomputer.applyMatrix(m);
  m.makeTranslation(0,    +0.25,0);  gKomputer.applyMatrix(m);
  m.makeTranslation(0,TopY/2   ,0);  gKomputer.merge(gStange, m);
  m.makeTranslation(0,TopY-0.01,0);  gKomputer.merge(gScheibe,m);
                                     gKomputer.merge(gScheibe);

	var gAuge = new THREE.SphereGeometry(0.05,8);   // IE:  TetrahedronGeometry  ist sehr langsamm!
  var gKeys = Box(  0.18,0.1,0.18);  // Tastatur
  var gCard = Box(  0.18,0.1,0.18);  // Tastatur

  m.makeRotationX(         g(-15));  gKeys.applyMatrix(m);
  m.makeTranslation(    0,1.25,-0.67 );  gKomputer.merge(gKeys,m,2);
  m.makeTranslation(-0.56,0.74, 0    );  gKomputer.merge(gCard,m,2);
  m.makeTranslation(    0,1.55,-0.545);  gKomputer.merge(gAuge,m,1);

  gKomputer.computeFaceNormals();   // Fl?chenfarbe einrichten
  gKomputer.computeVertexNormals(); // Farb?berg?nge weich
  gKomputer.mergeVertices()         // Nach V.Normals ist das ok :-)

  var   z = 0.2;  var   r = 4.1;
  var xEi = new THREE.MeshFaceMaterial( [ cTop, cYellow, cDark ] ); 
  var mEi = new_mesh_snap(gKomputer, xEi, '^', vBoden, 0,r,z)
  return mEi;
}


function Wand() {  /// WAND //////////// wwwwwwwwwww

  var x = 5.05 / 2; // Breite der 8 Teile (Extruder) geht nach 2 Seiten
  var s = 3.7;        // Grader Teil des Ux
  var a = g(12);    // Winkel, in dem der gerade Teil ansteigt (relativ zur waagerechten)
  var r = 0.2;      // Radius in der Ecke des U
  var w = g(360/8/2)  // Winkel der "Rautenseiten": verg??ert die Breite x um z*sin(w), verk?rzt die Tiefe als z*cos(w)
  var e = w * 0.6;    // Die Kaschiereckst?cke sind nicht ?ber den ganzen Eckwinkel
  var t = 16          // Teile der Rundung

  //// Wand ////
  gWand = new THREE.Geometry();
  // Das U beginnt mit dem unteren geraden Teil bei (relativ) 0,0:
  var y=0; var z=0; var v = -1;  // 0..2 sind schon oben da
  //x,y,z - Ausrichtung: x=Breite, y=H?he, z=V auf mich zu, 
  gWand.vertices.push( new THREE.Vector3(+x, y, z ), new THREE.Vector3( -x, y, z )); v+=2;  // vertices 0 und 1
  // So brechnet sich das andere Ende des unteren Teils:
  var sc = SinCos(a,s); y+= sc[0]; z+= sc[1]; // Es wird aber vom Bogen schon mit "gezeichnet"

  //  Mittelpunkt des Kreises:
  var sc = SinCos(a,r); y+= sc[1]; z-= sc[0];
  // Den Bogen beginnt/endet 90 Grad zu den geraden Teilen
  for(A = +a-g(90) ; A<= -a+g(90) ; A+= g(10) ) {
    var sc = SinCos(A,r); yy = y+sc[0]; zz = z+sc[1];
    var sc = SinCos(w,zz);
    gWand.vertices.push( new THREE.Vector3( +x+sc[0],  yy, sc[1]  ), 
                         new THREE.Vector3( -x-sc[0],  yy, sc[1] )); v+=2   // Beim ersten mal: vertices 2 und 3
    gWand.faces.push(    new THREE.Face3(  v-3, v-2, v-1 ), new THREE.Face3(  v-1, v-2, v )); // Beim ersten mal: faces 0,1,2 und 1,2,3 => -3-2-1 und -2-1-0
  }

  // Jetzt folgt der obere Gerade Teil:
  var sc = SinCos(a,r); y+= sc[1]; z+= sc[0]; // Der Anfang wird schon vom Bogen "gezeichnet"
  var sc = SinCos(a,s); y+= sc[0]; z+=-sc[1]; // Das Ende:
  gWand.vertices.push( new THREE.Vector3( +x,   y, z   ), new THREE.Vector3( -x,   y, z )); v+=2
  gWand.faces.push(    new THREE.Face3(  v-3, v-2, v-1 ), new THREE.Face3(  v-1, v-2, v ));
  gWand.computeFaceNormals();   // Fl?chenfarbe einrichten
  gWand.computeVertexNormals(); // Farb?berg?nge weich


  //// Ecke /////////////////

  s = 4-0.8;    // Grader Teil des U ist bei der Ecke k?rzer

  var rr = 0.6; var ru = 0.4;
  var dx = 0.5; // Breite der Rundung
  var dz = 0.5; // Dicke der Rundung
  var ar = g(35)
  var au = g(55)

  gEcke = new THREE.Geometry();
  // Das U beginnt mit dem unteren geraden Teil bei (relativ) 0,0:
  var y=0; var z=0; var v = -1;  // 0..2 sind schon oben da
  //x,y,z - Ausrichtung: x=Breite, y=H?he, z=V auf mich zu,
  gEcke.vertices.push( new THREE.Vector3(+0, y, z ), new THREE.Vector3( -0, y, z )); v+=2;  // vertices 0 und 1
 
  // Rundung statt Spitzen            
  for(A = g(15) ; A<= ar ; A+= g(5)) 
  { var xx =  Math.sin(A)   *rr;
    var zz = (Math.cos(A)-1)*rr;
    var yy = (z-zz)*Math.sin(a);
  gEcke.vertices.push( new THREE.Vector3( xx, y+yy, z-zz ), new THREE.Vector3( -xx, y+yy, z-zz )); v+=2
  gEcke.faces.push(    new THREE.Face3(  v-3, v-2,  v-1  ), new THREE.Face3(   v-1, v-2,  v    ));
  }

  // ?bergangsrundung
  for(A = au-g(15) ; A>= g(-9) ; A-= g(5)) 
  { 
    var sc = SinCos(A,ru);
    var xx = -sc[1]+dx;
    var zz = +sc[0]-dz;
    var yy = (z-zz)*Math.sin(a);
    gEcke.vertices.push( new THREE.Vector3( +xx, y+yy, z-zz ), new THREE.Vector3( -xx, y+yy, z-zz )); v+=2
    gEcke.faces.push(    new THREE.Face3(   v-3, v-2,  v-1  ), new THREE.Face3(   v-1,  v-2, v    ));
  }

  // So brechnet sich das andere Ende des unteren Teils:
  var sc = SinCos(a,s); y+= sc[0]; z+= sc[1]; // Es wird aber vom Bogen schon mit "gezeichnet"

  //  Mittelpunkt des Kreises:
  y+= r*+Math.cos(a); z-= r*Math.sin(a);
  // Den Bogen beginnt/endet 90 Grad zu den geraden Teilen
  for(A = +a-g(90) ; A<= -a+g(90) ; A+= g(10) ) {
    yy = y+Math.sin(A)*r;  zz = z+Math.cos(A)*r
    gEcke.vertices.push( new THREE.Vector3( +0+zz*Math.sin(e),  yy, zz*Math.cos(e) ),
                         new THREE.Vector3( -0-zz*Math.sin(e),  yy, zz*Math.cos(e) )); v+=2   // Beim ersten mal: vertices 2 und 3
    gEcke.faces.push(    new THREE.Face3(  v-3, v-2, v-1 ), new THREE.Face3(  v-1, v-2, v ));
  }

  a += g(5);    // Winkel, in dem der gerade Teil ansteigt (relativ zur waagerechten)


  // Jetzt folgt der obere Gerade Teil:   (Der Anfang wird schon vom Bogen "gezeichnet")
  y+= s*+Math.sin(a); z+=-s*Math.cos(a); // Das Ende:

  // ?bergangs-Rundung
  for(A = g(-9) ; A<= au ; A+= g(5)) {
    xx = Math.cos(A)*-ru +dx;
    zz = Math.sin(A)* ru -dz;
    yy = -(z-zz)*Math.sin(a);
    gEcke.vertices.push( new THREE.Vector3( +xx,   y+yy, z-zz   ), new THREE.Vector3( -xx,   y+yy, z-zz ));   v+=2
    gEcke.faces.push(    new THREE.Face3(  v-3, v-2, v-1 ), new THREE.Face3(  v-1, v-2, v ));
  }             

  // Rundung statt Spitzen
  for(A = ar ; A>= g(0) ; A-= g(5)) {
    xx =  Math.sin(A)   *rr;
    zz = (Math.cos(A)-1)*rr;
    yy = -(z-zz)*Math.sin(a);
  gEcke.vertices.push( new THREE.Vector3( +xx,   y+yy, z-zz   ), new THREE.Vector3( -xx,   y+yy, z-zz )); v+=2
  gEcke.faces.push(    new THREE.Face3(  v-3, v-2, v-1 ), new THREE.Face3(  v-1, v-2, v ));
  }

  gEcke.computeFaceNormals();   // Fl?chenfarbe einrichten
  gEcke.computeVertexNormals(); // Farb?berg?nge weich


  var r = TopR/AchtA+TopK; // Deckenradius/AchteckAusenNachInnen+K?sten=Wandkanten
  Spin(8,0,0,0, function (angle) {
            new_mesh_snap(gWand,   cWand, '.', vBoden, angle+g(45/2), r           ,0);
    var e = new_mesh_snap(gEcke,   cWeis, '.', vBoden, angle        , r/Math.cos(w),0);
        e.position.y += 0.1;
  }); 
} // Wand /// wwwwww


function Querlatten() {  ///////////////////////// Unten vor Wand QQQQ

  var segment = new THREE.Geometry(); // Leere Geometry in der St?nder und Latten zusammen gemischt werden.
  var Kurve = [ [0,0],[0,6],[34,13],[56,21],[76,31],[98,45],[113,58],[126,72],[134,86],[136,101],[142,94],[149,90],[158,89],[153,84],[142,73],[131,55],[125,34],[122,0] ];
  var pStand = [];  var pFolie = []; var f = 0.668 / 101; 
  for(var i=0; i<Kurve.length; i++) {
      var x  = Kurve[i][0] *f;  // durch Maximalwert gibt Normierung auf 1, dann auf Realmass
      var y  = Kurve[i][1] *f;
             pStand.push( new THREE.Vector2( x, y ));
    if(i>12) pFolie.push( new THREE.Vector2( x, y ));
  }

  var l1 = 1.149;  
  var l2 = 3.450 + 2* 63*f * Math.sin(g(22.5)); // L?nge, addiert mit dem Teil, der duch die Schr?ge dazu kommt
  var l3 = 4.595;
  var b  = 0.02;
  var r = -(TopR+0.18) ;  // Radius von Latten und Folie dahinter. Negativ, damit sie an der anderen Seite und so richtig herum stehen

  var shapeStand = new THREE.Shape( pStand );
  var shapeFolie = new THREE.Shape( pFolie );
  var gStand = new THREE.ExtrudeGeometry( shapeStand, {amount:  b, bevelEnabled: false} );
  var gFolie = new THREE.ExtrudeGeometry( shapeFolie, {amount: l3, bevelEnabled: false} );
  var m      = new THREE.Matrix4();
      m.makeTranslation(  0,0,-b/2);   gStand.applyMatrix(m); // Stand-Mitte der H?he wird Mittelpunt
      m.makeRotationY(         g(90)); gFolie.applyMatrix(m); // Foline-Rotation
      m.makeTranslation(   -l3/2,0,0); gFolie.applyMatrix(m);
 
  // Ma?e der Latten-St?nder  
  var ga = g(-360/16);
  var gz = g(+360/16);
  var gi = g(   0   );
  
  // St?nder zum Segment, positioniern und ggf. drehen
  function Stand(seitw,winkel) {
          c = gStand.clone();
          var n = 1 + 0.2*Math.sin(Math.abs(winkel))
          m.makeScale(n,1,1);                 c.applyMatrix(m);
          m.makeRotationY( g(90)+winkel );    c.applyMatrix(m);
          m.makeTranslation( seitw,0,0);      c.applyMatrix(m);
          segment.merge(                      c );
  }

  // die 5 St?nder, die aussen verdreht.
  Stand(-l2/2,gz);
  Stand(-l1  ,gi);
  Stand(  0  ,gi);
  Stand(+l1  ,gi);
  Stand(+l2/2,ga);

  //// Die 7 wagerehten Latten zum Lattenst?nder-Segment
  step = new THREE.BoxGeometry(1,1,1);  
  step.vertices[0].z = 0.4; // Vorne angepahsd
  step.vertices[5].z = 0.4;
  m.makeTranslation(0,0,-0.5); step.applyMatrix(m);  // 0-Punkt nach "Vorne"

  function Step(hoch,hinten, brett) { // x z / in Bildpunkten * f = Meter
    c = step.clone();
    m.makeScale(l2+ (hinten+brett)*f*Math.sin(g(45+5)), b, brett*f ); c.applyMatrix(m);
    m.makeTranslation( 0, hoch, -hinten*f ); c.applyMatrix(m);
    segment.merge( c ); 
  }

  Step(0.58,129, 15);
  Step(0.49,121, 12);
  Step(0.40,101, 24);
  Step(0.31, 74, 46);
  Step(0.22, 38, 76);
  Step(0.13, -7,106);
  Step(0.04,-63,151);

  //// Segment und Folie  8 mal im Kreis anordnen  
  Spin(8,0,0,0, function (angle,step) {
    if(step!=3 || go('3')) {
      new_mesh_snap(segment, cDark, '.', vBoden, angle+g(45/2),r,0);
      new_mesh_snap(gFolie,  cWeis, '.', vBoden, angle+g(45/2),r+0.0,0);
    }
  });
  
} // Latten ///
               

function Hallenboden() {
  var cNice  = new THREE.MeshLambertMaterial( {color: 0xAaAaAa} );
  var gPlane = new THREE.PlaneGeometry(99,99,11,11);   //  IE: PlaneGeometry, kein PlaneGeometry
  var  plane = new THREE.Mesh(gPlane,cNice);
       plane.position.y =  FloorY;
       plane.rotation.x =  g(-90);
       tile.add(plane);
}



function Monitorwand() { // mmmm
 
  //// Monitore
  var wx = 0.53; var lx = 0.48; var fx = 0.03; z = 0.60;
  var wy = 0.53; var ly = 0.38; var fy = 0.02; u = (wy-ly)-(wx-lx);

  var m        = new THREE.Matrix4();
  var sMonitor =           SquareShape(-wx/2, 0, +wx/2,   wy );
      sMonitor.holes.push( SquareShape(-lx/2, u, +lx/2, u+ly ));
  var gMonitor = new THREE.ExtrudeGeometry( sMonitor, {amount: z, bevelEnabled: false } );
  var gFuss1 = new THREE.BoxGeometry(fx,fy,z);
  var gFuss2 = gFuss1.clone();
      m.makeTranslation(  wx/2-fx/2, -fy/2, z/2); gFuss1.applyMatrix(m); gMonitor.merge(gFuss1);
      m.makeTranslation( -wx/2+fx/2, -fy/2, z/2); gFuss2.applyMatrix(m); gMonitor.merge(gFuss2);

  var gRoehre = new THREE.PlaneGeometry( -lx, ly ); 
      m.makeTranslation(  0, ly/2+u,0.05); gRoehre.applyMatrix(m);// gMonitor.merge(gRoehre);

  var cHasso   = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/hasso.png' ) });  cHasso.name = "cHasso"
  var cMario   = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/mario.png') });  cMario.name = "cMario"  // Hilger 
  var cCliff   = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/cliff.png' ) });  cCliff.name = "cCliff" 
  var cSinus   = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/sinus.png' ) });  cCliff.name = "cSinus" 
  var cLinks   = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/links.png' ),transparent: true, opacity: 1   });  cLinks.name  = "cLinks" 
  var cRechts  = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture( '../pics/rechts.png'),transparent: true, opacity: 1   });  cRechts.name = "cRechts" 

      mMonitor = new THREE.Mesh( gMonitor, cBlack );
      mRoehre  = new THREE.Mesh( gRoehre,  cHasso );  mRoehre.name = "mRoehre"
      mMonitor.add(mRoehre);
  var aa = 0.45; var a = 180+aa; var p = 10-0.1; var r = TopR+1.15; var b = 10.2;
  var x = new_mesh_snap(mMonitor,  0, '^', vBoden, g(a-b), r, 0, g(-15));   x.position.y = TopY-0.85;
  var y = new_mesh_snap(mMonitor,  0, '^', vBoden, g(a  ), r, 0, g(-15));   y.position.y = TopY-0.85;
  var z = new_mesh_snap(mMonitor,  0, '^', vBoden, g(a+b), r, 0, g(-15));   z.position.y = TopY-0.85; z.name = "mMonitorZ"
      x.children[0].material = cMario;
      y.children[0].material = cCliff;

  //// Blenden vor/um Monitore
  var m     = new THREE.Matrix4();
  var h=0.1; var w=26; var b=w*h; d = 0.015;

  var gStacheln = new THREE.PlaneGeometry( b, h ); 
  var texture = THREE.ImageUtils.loadTexture('../pics/stachel.png')
      texture.wrapS = THREE.RepeatWrapping;
      texture.wrapT = THREE.RepeatWrapping;
      texture.repeat.set( w,1 );
      
  var cStacheln = new THREE.MeshPhongMaterial( {color: 0xf0f0f0, transparent: true, opacity: 0.6} );  
  //  cStacheln.map     = texture;
      cStacheln.bumpMap = texture;
      cStacheln.bumpScale = 0.06;
  var x = new_mesh_snap(gStacheln,cStacheln,'^', vBoden, g(aa),-r+0.15, 0, g( 15));    x.position.y = TopY-0.82;

  var gRand = new THREE.Geometry(); // oder: http://www.smartjava.org/ltjs/chapter-06/04-extrude-tube.html
  var gSchlauch = new THREE.CylinderGeometry(d,d,h+d*2);
      m.makeTranslation( b/2,0,0); gSchlauch.applyMatrix(m); gRand.merge(gSchlauch);
      m.makeTranslation(-b,  0,0); gSchlauch.applyMatrix(m); gRand.merge(gSchlauch);
  var gSchlauch = new THREE.CylinderGeometry(d,d,b-d*2);
      m.makeRotationZ(    g(-90)); gSchlauch.applyMatrix(m);
      m.makeTranslation(0, h/2,0); gSchlauch.applyMatrix(m); gRand.merge(gSchlauch);
      m.makeTranslation(0,-h,  0); gSchlauch.applyMatrix(m); gRand.merge(gSchlauch);
      
  var x = new_mesh_snap(gRand,cGlas,'^', vBoden, g(aa),-r+0.15, 0, g( 15));    x.position.y = TopY-0.82;
  adjust = x;

/*var gBlende = new THREE.PlaneGeometry( 27*0.1, 0.5 );   // Veraltert, f?r IE n?tig?
      gBlende.vertices[0].x -= 0.18;
      gBlende.vertices[1].x += 0.18;*/
  var gBlende = new THREE.PlaneGeometry( 27*0.1, 0.5 ); 
//??      gBlende.attributes.position.array[0] -= 0.18;
//??      gBlende.attributes.position.array[3] += 0.18;
  var x = new_mesh_snap(gBlende,  cMatt,'^', vBoden, g(aa),-r-0.00, 0, g( 15));    x.position.y = TopY-0.57;

  // 525p = 2.22m => 236p/m 
  var by = 0.006; var bx=0.50; var bz=bx/7; // 118x8 => 50x0.03
  var lxz = 0.03; var ly1 = 2.40; var ly2 = 1.40; var a1 = 33; var a2 = 26; var a3 = -4.5; var h1 = 0; var h2 = -20;
  var gGestell = new THREE.BoxGeometry(bx,by,bz);
      m.makeTranslation(0, by/2,0); gGestell.applyMatrix(m);

  //// Gestell unten //////////////////////////////////////////

  /// Stangen (rund)
  function Latte(y,a,h,t) {
  var gLatte   = new THREE.CylinderGeometry(lxz/2,lxz/2,y);
      m.makeTranslation(0, y/2,0); gLatte.applyMatrix(m);
      m.makeRotationZ(      g(a)); gLatte.applyMatrix(m);
      m.makeRotationX(      g(h)); gLatte.applyMatrix(m);
      m.makeTranslation(t,   0,0); gLatte.applyMatrix(m);
      return gLatte;
  }
  gGestell.merge(Latte(ly1,+a1,h1,-bx/4));  gGestell.merge(Latte(ly2 ,+a2,h2,-bx/4));  gGestell.merge(Latte(ly2 ,+a3,h2,-bx/4));
  gGestell.merge(Latte(ly1,-a1,h1,+bx/4));  gGestell.merge(Latte(ly2 ,-a2,h2,+bx/4));  gGestell.merge(Latte(ly2 ,-a3,h2,+bx/4));
  var x = new_mesh_snap(gGestell, cGrey,'^', vBoden, 0,-r-0.5, 0);   x.position.y = 0;      

  /// Glasfl?che
  var d  = g(90)   // Direktion hoch ist +90 als Anfangsorientierung
  var w1 = g(10)   // Start-/Endwinkel oben 90+/-
  var w2 = g(12.4) //                  unten
  var r1 = 6.03    // Radius oben
  var r2 = r1/1.8  //        unten
  // ein .arg ist relativ zm aktuellen X/Y! Damit der Anfang = dem aktuellen X/Y entspricht, mu? der Nullpunkt verschoben werden
  var y0 = 1.4;          // Starth?he oben
  var y  = 0.5           // H?he an der Seite
  var x1 = r1*Math.sin(w1); var x2 = r2*Math.sin(w2);
  var y1 = r1*Math.cos(w1); var y2 = r2*Math.cos(w2);
  //console.log(x1*2);   // Breite sollte 2.22m sein? 

  var sGlas = new THREE.Shape();
      sGlas.moveTo(  -x1,  y0                                      ); // Absolut? in Shape-Koordinaten
      //   .arc   (   aX,  aY, aRadius, aStartA, aEndA, aClockwise );
      sGlas.arc   (   x1, -y1,      r1,   +d+w1, +d-w1,       true ); // Relativ zum MoveTo-Ziel?
      sGlas.moveTo(   x2,  y0-y                                    ); // Absolut?
      sGlas.arc   (  -x2,  y2,      r2,   -d+w2, -d-w2,       true ); // Relaiiv
  var gGlas = new THREE.ExtrudeGeometry( sGlas, {amount:0.005, bevelEnabled: false } );
      new_mesh_snap(gGlas, cCone,'.', vBoden, 0,-r-0.5+0.015 , 0);

  /// Milchfl?he  
  var w3 = g(20) // Start-/Endwinkel oben 90+/-
  var w4 = g(19) //                  unten
  var r3 = 1.1   // Radius oben
  var r4 = r3*1.7  //        unten
  var x3 = r3*Math.sin(w3); var x4 = r4*Math.sin(w4);
  var y3 = r3*Math.cos(w3); var y4 = r4*Math.cos(w4);
  var x  = (x1-x2);
  var sMilch = new THREE.Shape();
      sMilch.moveTo(  -x1,   y0                                      );
      sMilch.arc   (   x1,  -y3-0.02, r3,   +d+w3, +d-w3,       true );
      sMilch.lineTo(   x1,   y0                                      );
      sMilch.lineTo(   x1-x, y0-y                                    );
      sMilch.arc   (   x-x1, y4+0.08, r4,   -d+w4, -d-w4+0.02/*???*/,       true );
      sMilch.lineTo(  -x1+x, y0-y                                    );

  /// -Monitorloch
  var d  = g(90)  // Direktion hoch ist +90 als Anfangsorientierung
  var w1 = g(9)  // Start-/Endwinkel oben 90+/-
  var w2 = g(10)  //                  unten
  var r1 = 1.2    // 1.2  Radius oben
  var r2 = r1/1.5 //        unten
  var x5 = r1*Math.sin(w1); var x6 = r2*Math.cos(w2);
  var y5 = r1*Math.cos(w1); var y6 = r2*Math.sin(w2);
  var x  = 2*x5-(x5-x6); // Damit unterer Bogen mitten unter den Oberen kommt,
  var y  = r1/12  // H?he an der Seite
  var sMonLoch = new THREE.Path();
      sMonLoch.moveTo(  +x5,y0-0.05                                      );  // gX=zur Mitte x1=Zum Loch-Anfang an der Seite 
      //      .arc   (   aX,  aY, aRadius, aStartA, aEndA, aClockwise );  WICHTIG!:  Ggen den Uhrzeigersinn
      sMonLoch.arc   (  -x5, -y5,      r1,   +d-w1, +d+w1,       true );
      sMonLoch.arc   (   x6, -y6,      r2,  2*d-w2,2*d+w2,       true ); 
      sMonLoch.arc   (   x5,  y5,      r1,   -d-w1, -d+w1,       true );
      sMonLoch.arc   (  -x6,  y6,      r2,     -w2,   +w2,       true );
      sMilch.holes.push(sMonLoch);

  var gSinus   = new THREE.PlaneGeometry( 2.3*x5,2.3*y6 ); 
      m.makeTranslation( 0, y0-0.19 ,0.001); gSinus.applyMatrix(m);
      mSinus   = new THREE.Mesh( gSinus,  cSinus );

  // Loch Links
  var y = y0-0.1; var yr = 0.29; var yu = 0.02; var yo = 0.04;
  var x = -0.31;  var xo = 0.65; var xu = 0.42;
  var sLinks = new THREE.Path();
      sLinks.moveTo( x,    y       ); // Rechts oben
      sLinks.lineTo( x-xo, y+yo    );
      sLinks.lineTo( x-xu, y-yr-yu );
      sLinks.lineTo( x,    y-yr    );
      sMilch.holes.push(sLinks);
  var gLinks = new THREE.PlaneGeometry( xo*1.05,(yr+yo+yu)*1.05 ); 
      m.makeTranslation( x-xo/2, y-(yr)/2+0.007 ,0.002); gLinks.applyMatrix(m);
      mLinks = new THREE.Mesh( gLinks, cLinks );

  // Loch Rechts
  var y = y0-0.07; var yl = 0.29; var yu = 0.02; var ym = 0.18; var yo = 0.02;
  var x = 0.32;    var xo = 0.41; var xm = 0.26; var xn = 0.33; var xu = 0.21;
  var sRechts = new THREE.Shape();
      sRechts.moveTo( x,    y       ); // Links oben
      sRechts.lineTo( x,    y-yr    );
      sRechts.lineTo( x+xu, y-yr-yu );
      sRechts.lineTo( x+xm, y-ym    );
      sRechts.lineTo( x+xn, y-ym    );
      sRechts.lineTo( x+xo, y+yo    );
      sMilch.holes.push(sRechts);
  var gRechts = new THREE.PlaneGeometry( xo*1.05,(yl+yo+yu)*1.05 ); 
      m.makeTranslation( x+xo/2, y-(yr)/2 ,0.002); gRechts.applyMatrix(m);
      mRechts = new THREE.Mesh( gRechts,cRechts );

  var gMilch = new THREE.ExtrudeGeometry( sMilch, {amount:0.005, bevelEnabled: false } );
  var x = new_mesh_snap(gMilch, cGrey,'.', vBoden, 0,-r-0.5+0.020, 0);// cGrey
      x.add(mSinus);
      x.add(mLinks);
      x.add(mRechts);

  // Kugeln
	var gKugeln = new THREE.Geometry;  var d = 0.047; var k = 0.03; var ym = 0.98; var yu = 0.855; var yo = 1.415;
	var gKugel  = new THREE.SphereGeometry(d/2,d/2);
	var gKugeK  = new THREE.SphereGeometry(k/2,k/2);
      gK = gKugel.clone(); m.makeTranslation( +d*1.5, ym, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
      gK = gKugel.clone(); m.makeTranslation( +d*0.5, ym, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
      gK = gKugel.clone(); m.makeTranslation( -d*0.5, ym, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
      gK = gKugel.clone(); m.makeTranslation( -d*1.5, ym, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);

			gK = gKugel.clone(); m.makeTranslation(  0.56, yu+0.065,0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation(  0.33, yu+.020, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation(  0.18, yu+.005, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation( -0.10, yu     , 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation( -0.31, yu+0.02, 0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugel.clone(); m.makeTranslation( -0.56, yu+0.065,0 ); gK.applyMatrix(m); gKugeln.merge(gK);

			gK = gKugeK.clone(); m.makeTranslation( -0.75, yo+0.0 ,0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation( -0.32, yo+0.03 ,0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation(  0.30, yo+0.03 ,0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation(  0.43, yo+0.0 ,0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			gK = gKugeK.clone(); m.makeTranslation(  0.64, yo+0.0 ,0 ); gK.applyMatrix(m); gKugeln.merge(gK);
			
			
    	new_mesh_snap(gKugeln, cLight, '.',vBoden,0,-r-0.5+0.040,0);  

	
			
			
}



// / /   M A I N   / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / / //

function OrionKK() {

  Orion_Init();
  // Mittelpunkt der Decke und des Bodens relativ zu denen alle anderen Objekte Plaziert werden.
  vDecke = new THREE.Object3D().translateY(0.7+TopY); vDecke.name = "vDecke"; tile.add(vDecke);
  vBoden = new THREE.Object3D().translateY(0.7     ); vBoden.name = "vBoden"; tile.add(vBoden);


  
  if(go('d'))   Decke();
  if(go('b'))   Boden();

  if(go('a'))   Aufzug();
  if(go('c'))   Cones();      // IE: hier wird es langsam wegen Canvas
  if(go('l'))   Lamellen();   //     "
	
  if(go('k')) {
      mStachelei = Stachelei();  //   mStachelei.rotation.y = g(-3);
      mComputer  = Komputer();
  }
  
  if(go('w')) { Wand();  
  if(go('q'))   Querlatten(); } 
  if(go('e')) ; // Loecher und Kuppeln darstellen

  if(go('m'))   Monitorwand();

//mComputer.visible = false;

}

// file:///Users/Karl/Dropbox/ac1000_MZ4/tr.html?dbcaokwl






//######### - Orion.js ###############################################################################################################

























var xOrion = -25;
var zOrion = -25;
var yOrion =   0;


var getSceneVersion = function() { return "Karlos Orion-Baustelle w_karlos.js V15.12.29"; };

//var geometry = new THREE.TextGeometry("heim", {height:10});


	
function OctagonColumn(xy, z)
	{
	  geom = new THREE.CylinderGeometry(xy/2, xy/2, z, 8, 1);
		mesh = new THREE.Mesh( geom,cYellow );
	  mesh.position.x=xOrion;
	  mesh.position.z=zOrion;
	  mesh.position.y=yOrion+ z/2;
		tile.add(mesh);yOrion+=z;
	}
	
function LiftKuppeln(LiftUnten_xy, LiftUnten_z)
	{
	    Sichtbar  = LiftUnten_z/5;   // Sichtbareer Durchmesser der Kugeln
	    Abstand   = Sichtbar/4;             // Abstand zwischen Kugeln
	    Reihe1 =           Sichtbar/2 + Abstand/1;
	    Reihe2 = Reihe1  + Sichtbar   + Abstand;
	  //r;

	    Kuppelzahl = 4;  // mal 2 Ebenen
	    for( r=22.5; r<360; r=r+(360.0/Kuppelzahl) )
	    {
	      //LiftSphere( r, <Sichtbar,Sichtbar,0.01>, <LiftUnten_xy/2* llSin(r*DEG_TO_RAD), LiftUnten_xy/2* -llCos(r*DEG_TO_RAD), Reihe1>);
				  x=3.7
				  geom = new THREE.SphereGeometry(Sichtbar,32,32);
					mesh = new THREE.Mesh( geom,cBlack );
				  mesh.position.x=xOrion+LiftUnten_xy/x* +Math.sin(g(r));
				  mesh.position.z=zOrion+LiftUnten_xy/x* -Math.cos(g(r));
				  mesh.position.y=Reihe1;
					tile.add( mesh );

				  geom = new THREE.SphereGeometry(Sichtbar,32,32);
					mesh = new THREE.Mesh( geom,cBlack );
				  mesh.position.x=xOrion+LiftUnten_xy/x* +Math.sin(g(r));
				  mesh.position.z=zOrion+LiftUnten_xy/x* -Math.cos(g(r));
				  mesh.position.y=Reihe2;
					tile.add( mesh );
	    }


	}



function UnterAchteck(xzs,z )
	{

		  geom = new THREE.CylinderGeometry(xzs/2, 0, z*1.3, 8, 1 );
			mesh = new THREE.Mesh( geom,cYellow );
		  mesh.position.x=xOrion;
		  mesh.position.z=zOrion;
		  mesh.position.y=yOrion+ z/2;
			tile.add(mesh);yOrion+=z*1.15;
	}



function Antriebsring(m)
	{
	    scheibe_z   = 0.8*m;
	    m/=2
	    scheibe_xy1 = 131*m;
	    scheibe_xy6 = 138*m;
	    scheibe_xy4 = 170*m;
	    scheibe_xy2 = 142*m;

	    // Mittel-Hauptseule
		  geom = new THREE.CylinderGeometry(scheibe_xy1, scheibe_xy1, scheibe_z*7, 8);
			mesh = new THREE.Mesh( geom, cGlas );
		  mesh.position.x=xOrion;
		  mesh.position.z=zOrion;
		  mesh.position.y=yOrion+ scheibe_z*7/2;
			tile.add(mesh);yOrion+=scheibe_z*7/2;

	    // Scheibe oben
		  geom = new THREE.CylinderGeometry(scheibe_xy6, scheibe_xy6, scheibe_z, 64);
			mesh = new THREE.Mesh( geom, cGlas );
		  mesh.position.x=xOrion;
		  mesh.position.z=zOrion;
		  mesh.position.y=yOrion+ scheibe_z*2;
			tile.add(mesh);

	    // Mittelscheibe
		  geom = new THREE.CylinderGeometry(scheibe_xy4, scheibe_xy4, scheibe_z, 64);
			mesh = new THREE.Mesh( geom, cGlas );
		  mesh.position.x=xOrion;
		  mesh.position.z=zOrion;
		  mesh.position.y=yOrion;
			tile.add(mesh);

	    // Scheibe unten
		  geom = new THREE.CylinderGeometry(scheibe_xy2, scheibe_xy2, scheibe_z,  8);
			mesh = new THREE.Mesh( geom, cGlas );
		  mesh.position.x=xOrion;
		  mesh.position.z=zOrion;
		  mesh.position.y=yOrion+ -scheibe_z*2;
			tile.add(mesh);
/**/
	}


function OrionAussen(m) // m=Masstab, kann man aber auch als Meter lesen
	{
		     yOrion =   0;
	       Spanten_x    = 95.0*m;         Spantenzahl  =  8;
	       Achteck_xy   =150.0*m;         Achteck_z    = 10.8*m;
	       LiftOben__xy = 17.2*m;         LiftOben__z  = 14.8*m;
	       LiftMitte_xy = 14.8*m;         LiftMitte_z  = 13.8*m;
	       LiftUnten_xy = 12.3*m;         LiftUnten_z  = 13.8*m;

		  if(1)
		  {
		    LiftKuppeln(LiftUnten_xy, LiftUnten_z);

		    // *** Drei Lift-Achtecke
		    OctagonColumn( LiftUnten_xy, LiftUnten_z );
		    OctagonColumn( LiftMitte_xy, LiftMitte_z );
		    OctagonColumn( LiftOben__xy, LiftOben__z );
		  }

	    // *** Achteck-Kegel
	    UnterAchteck( Achteck_xy, Achteck_z);

	    // Achteck-Kanten liegen 1/2 Achtel versetzt von 0 Grad
	    K2 = Spanten_x/1.8;
	    for( r=360.0/Spantenzahl/2; r<360; r=r+(360.0/Spantenzahl) )
	      ;//Spante( r, Spanten_x, <K2* llSin(r*DEG_TO_RAD), K2* -llCos(r*DEG_TO_RAD), Spanten_x/200>);

	    Antriebsring(m);
	  //Oberschiff(m);

	}


function buildScene() {

	  tile = new THREE.Mesh();
	  tile.add(buildGround('../tex/kacheln512.jpg'));

	  var cBlack   = new THREE.MeshBasicMaterial( {color: 0x000000} );
	  var cYellow  = new THREE.MeshBasicMaterial( {color: 0xe0e000} );
		var material = new THREE.MeshBasicMaterial( {color: 0x00ff00} );
	  var cGlas    = new THREE.MeshBasicMaterial( {color: 0xf0f0f0, transparent: true, opacity: 0.6} );  

	  var geom = null;
		var mesh = null;
	
	
  OrionKK();

  // Orion in klein und Gross
	var mOrion = 1/20;
	xOrion = -15;
	zOrion = -10;
//OrionAussen(mOrion);
	
  xOrion = -25;
  zOrion = -25;
//OrionAussen(1);


  // "Huegel zum Testen der H�he
  geom = new THREE.CylinderGeometry(0, bb, cc, 4, 1);
	mesh = new THREE.Mesh( geom,material );
  mesh.position.x=aa;
  mesh.position.z=aa;
  mesh.position.y=  0.5;
	mesh.rotation.y=g(360/4/2)
	tile.add( mesh );


  // Szene aus Skript nachladen
      vscName = "w_karl.vsc";
      xchgPosInfo("rdfile.php?load=" + vscName, 1);

  // THE END
  return tile;
}


var oOrtung = undefined


function recvSceneData(rxText) {
  // console.log(  "----------- VSC-Recv");
	if (vscDemoObj === undefined) {
      vscDemoObj = buildVscScene(vscName, rxText);
      vscDemoObj.name = name;
      scene.add(vscDemoObj);
	    //console.log("*********** VSC-Recv: ",vscName," Text|",rxText);			
	    //console.log("*********** vscDemoObj: ",vscDemoObj);			

			Boxes     = getVscBoundingBoxes(name);
			BoxFrames = showBoundBoxes(Boxes);
			scene.add(BoxFrames);
			 
      // Alle bekommen den Instanznamen "OrionAussen". Bei der Suche mit getObjectByName wird das erste gefunden!
      var oa = vscDemoObj.getObjectByName("OrionAussen");
			if( oa!==undefined) {
		    oOrtung = oa.getObjectByName("Ortung");
 	     //nsole.log("*********** oa",oa);
 	     console.log("*********** oOrtung",oOrtung);
			}



	}
}



function cycleScene(msPerFrame) {

	if (oOrtung !== undefined) {
		  oOrtung.rotation.y = oOrtung.rotation.y - 0.001 * msPerFrame
      // console.log("*********** Ortung: ",oOrtung.rotation.y);			
	}

}

function testScenePosition(xp, yp, zp) {

	if (Boxes !== undefined) {
		if (!testBoundBoxFree(Boxes, xp, yp, zp))
			return false;
	}

	return true;
}

function getSceneGroundLevel(xp, yp, zp) {
	if( Math.abs(xp-aa)<bb/2		// Auf der Pyramide?
	&&  Math.abs(zp-aa)<bb/2 )
		{
         var x = bb/2-Math.abs(xp-aa)	// Abstand von xMitte=>y0, xMitte=>yMitte
         var z = bb/2-Math.abs(zp-aa)
			   var y = Math.min(x,z)				// Die Kleinere H�he z�hlt
			   var yy= y*cc/(bb/2)					// Von Abstand-H�he auf tats�chliche H�he umrechen.
				 // console.log("x,z,y,yy", x,z,y,yy);			
				 return yy;
		} 

	if (Boxes !== undefined) {
		return getBoundBoxGroundLevel(Boxes, xp, yp, zp);
	}


	return 0;
}
